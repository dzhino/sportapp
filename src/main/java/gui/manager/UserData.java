package gui.manager;

public class UserData {
    private static int UserId;
    private static String FirstName;
    private static String SecondName;
    private static String ThirdName;
    private static String Email;
    private static String PhoneNumber;
    private static int RoleId;

    public UserData(int userId, String firstName, String secondName, String thirdName, String email, String phoneNumber, int roleId) {
        UserId = userId;
        FirstName = firstName;
        SecondName = secondName;
        ThirdName = thirdName;
        Email = email;
        PhoneNumber = phoneNumber;
        RoleId = roleId;
    }

    public static int getUserId() {
        return UserId;
    }

    public static String getFirstName() {
        return FirstName;
    }

    public static String getSecondName() {
        return SecondName;
    }

    public static String getThirdName() {
        return ThirdName;
    }

    public static String getEmail() {
        return Email;
    }

    public static String getPhoneNumber() {
        return PhoneNumber;
    }

    public static int getRoleId() {
        return RoleId;
    }
}
