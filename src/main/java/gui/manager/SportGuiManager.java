package gui.manager;

import database.DatabaseOperations;
import database.Roles;
import gui.view.*;

import java.awt.*;

public class SportGuiManager {
    private static MainFrame mainFrame;
    private static String LastView;
    private static String FutureView;

    private static final String SECTIONS_VIEW = "SectionsView";
    private static final String SPORTSMEN_SECTIONS_VIEW = "SportsmenSectionView";
    private static final String COACH_SECTIONS_VIEW = "CoachSectionView";
    private static final String COMPETITIONS_VIEW = "CompetitionsView";
    private static final String FUTURE_COMPETITION_VIEW = "Future Competition";
    private static final String LAST_COMPETITION_VIEW = "Last Competition";

    public static void setFutureView(String futureView) {
        FutureView = futureView;
    }

    public static String getFutureView() {
        return FutureView;
    }

    public static String getFutureCompetitionView() {
        return FUTURE_COMPETITION_VIEW;
    }

    public static String getLastCompetitionView() {
        return LAST_COMPETITION_VIEW;
    }

    public static String getSectionsView(){
        return SECTIONS_VIEW;
    }

    public static String getSportsmenSectionsView() {
        return SPORTSMEN_SECTIONS_VIEW;
    }

    public static String getCoachSectionsView() {
        return COACH_SECTIONS_VIEW;
    }

    public static String getLastView(){
        return LastView;
    }

    public static void setLastView(String lastView) {
        LastView = lastView;
    }


    public static void putMainFrameToCenter() {
        Dimension frameSize = mainFrame.getSize();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        mainFrame.setBounds((screenSize.width - frameSize.width)/2, (screenSize.height - frameSize.height)/2, frameSize.width, frameSize.height);
    }

    public static String getCompetitionsView() {
        return COMPETITIONS_VIEW;
    }

    public void start() {
        mainFrame = new MainFrame();
        mainFrame.add(new StartView(mainFrame));
        mainFrame.setVisible(true);
    }

    public static void chooseNextView() {

        if (UserData.getRoleId() == Roles.SPORTSMEN.mineOrdinal()) {
            mainFrame.add(new SportsmenView(mainFrame));

        } else if (UserData.getRoleId() == Roles.DIRECTOR.mineOrdinal()) {
            if (!DatabaseOperations.isSportStructureCreated()){
                mainFrame.add(new CreateStructureView(mainFrame));
            } else {
                mainFrame.add(new DirectorView(mainFrame));
            }

        } else if (UserData.getRoleId() == Roles.ORGANIZER.mineOrdinal()) {
            mainFrame.add(new OrganizerView(mainFrame));

        } else if (UserData.getRoleId() == Roles.COACH.mineOrdinal()){
            mainFrame.add(new CoachView(mainFrame));
        }
    }
    public static void closeWindow() {
        mainFrame.getContentPane().removeAll();
    }
}
