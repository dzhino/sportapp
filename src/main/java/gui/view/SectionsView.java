package gui.view;

import database.DatabaseOperations;
import gui.manager.SportGuiManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class SectionsView extends JPanel {

    private MainFrame mainFrame;
    private ArrayList<ArrayList<String>> sections;
    private ArrayList<JButton> scheduleButtons = new ArrayList<>();
    private ArrayList<JButton> coachButtons = new ArrayList<>();
    private ArrayList<JButton> joinButtons = new ArrayList<>();

    private final int OPTIONS_COUNT;
    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    private static final String CANCEL_BUTTON = "Cancel";
    private static final String COACH_BUTTON = "Coach's contacts";
    private static final String SCHEDULE_BUTTON = "Schedule";
    private static final String JOIN_BUTTON = "Join";

    SectionsView(MainFrame mainFrame, ArrayList<ArrayList<String>> sections){
        this.mainFrame = mainFrame;
        this.sections = sections;
        OPTIONS_COUNT = sections.size() + 2;
        initMainPanel();
        SportGuiManager.putMainFrameToCenter();
        mainFrame.setVisible(true);
    }
    private void initMainPanel() {
        final JPanel mainPanel = new JPanel(new GridLayout (OPTIONS_COUNT, 1));
        final JPanel[] userDataPanels = new JPanel[OPTIONS_COUNT];

        ButtonListener buttonListener = new ButtonListener();

        for (int i = 0; i < OPTIONS_COUNT - 2; i++) {
            userDataPanels[i] = new JPanel(new GridLayout(1, sections.get(i).size() + 1));

            for(int j = 1; j < sections.get(i).size(); j++)
            {
                JTextField stringField = new JTextField(sections.get(i).get(j), sections.get(i).get(j).length());
                stringField.setEnabled(false);
                userDataPanels[i].add(stringField);
            }


            coachButtons.add(new JButton(COACH_BUTTON));
            scheduleButtons.add(new JButton(SCHEDULE_BUTTON));
            joinButtons.add(new JButton(JOIN_BUTTON));

            coachButtons.get(i).addActionListener(buttonListener);
            scheduleButtons.get(i).addActionListener(buttonListener);
            joinButtons.get(i).addActionListener(buttonListener);

            userDataPanels[i].add(coachButtons.get(i));
            userDataPanels[i].add(scheduleButtons.get(i));
            userDataPanels[i].add(joinButtons.get(i));

            mainPanel.add(userDataPanels[i]);
        }

        JButton cancelButton = new JButton(CANCEL_BUTTON);
        userDataPanels[OPTIONS_COUNT - 1] = new JPanel(new GridLayout(1, 1));
        userDataPanels[OPTIONS_COUNT - 1].add(cancelButton);

        mainPanel.add(userDataPanels[OPTIONS_COUNT - 1]);
        cancelButton.addActionListener(buttonListener);
        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        mainFrame.setResizable(false);
    }

    private class ButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton)e.getSource();
            if(button.getText().equals(CANCEL_BUTTON)) {
                SportGuiManager.closeWindow();
                mainFrame.add(new ChooseSportTypeView(mainFrame));
                mainFrame.validate();
            }
            for (int i = 0; i < coachButtons.size(); i++) {
                if (coachButtons.get(i).equals(button)) {
                    SportGuiManager.closeWindow();
                    SportGuiManager.setLastView(SportGuiManager.getSectionsView());
                    mainFrame.add(new CoachInfoView (mainFrame, sections, i));
                    mainFrame.validate();
                }
            }
            for (int i = 0; i < scheduleButtons.size(); i++) {
                if (scheduleButtons.get(i).equals(button)) {
                    SportGuiManager.closeWindow();
                    SportGuiManager.setLastView(SportGuiManager.getSectionsView());
                    mainFrame.add(new ScheduleView (mainFrame, sections, i));
                    mainFrame.validate();
                }
            }
            for (int i = 0; i < joinButtons.size(); i++) {
                if (joinButtons.get(i).equals(button)) {
                    SportGuiManager.closeWindow();
                    DatabaseOperations.joinSection(sections.get(i).get(0));
                    JOptionPane.showMessageDialog(SectionsView.this, "You joined to section");
                    mainFrame.add(new SportsmenView(mainFrame));
                    mainFrame.validate();
                }
            }
        }
    }
}
