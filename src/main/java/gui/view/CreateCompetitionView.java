package gui.view;

import database.DatabaseOperations;
import gui.manager.SportGuiManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.util.ArrayList;

class CreateCompetitionView extends JPanel {

    private static final String REGISTRATION_VIEW_TITLE = "Registration";
    private static final String EMPTY_STRING = "";
    private ArrayList<ArrayList<String>> places;
    private ArrayList<String> sportTypes;

    private static final String COMPETITION_NAME_LABEL = "Competition Name";
    private static final String PLACE_LABEL = "Place";
    private static final String SPORT_TYPE_LABEL = "Sport Type";
    private static final String START_DATE_LABEL = "Start Date";
    private static final String END_DATE_LABEL = "End Date";
    private static final String OK_BUTTON_LABEL = "OK";
    private static final String CANCEL_BUTTON_LABEL = "Cancel";

    final private JTextField nameTextField = new JTextField(TEXT_FIELD_COLUMNS_COUNT);
    private JComboBox sportTypeBox;
    private JComboBox placeBox;
    private JComboBox dayBox;
    private JComboBox monthBox;
    private JComboBox yearBox;
    private JComboBox minutesBox;
    private JComboBox hourBox;
    private JComboBox dayBox2;
    private JComboBox monthBox2;
    private JComboBox yearBox2;
    private JComboBox minutesBox2;
    private JComboBox hourBox2;


    private static final int OPTIONS_COUNT = 5;
    private static final int TEXT_FIELD_COLUMNS_COUNT = 15;
    private static final int FRAME_WIDTH = 600;
    private static final int FRAME_HEIGHT = 400;
    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    private MainFrame mainFrame;

    CreateCompetitionView(MainFrame mainFrame) {
        this.mainFrame = mainFrame;

        mainFrame.setTitle(REGISTRATION_VIEW_TITLE);
        mainFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        initMainPanel();
        initButtonPanel();
        mainFrame.pack();

        mainFrame.setResizable(false);
        mainFrame.setVisible(true);
    }

    private void initMainPanel() {
        final JPanel mainPanel = new JPanel(new GridLayout(OPTIONS_COUNT, 1));
        final JPanel[] userDataPanels = new JPanel[OPTIONS_COUNT];
        final JLabel[] userDataLabels = new JLabel[]{new JLabel(COMPETITION_NAME_LABEL),
                new JLabel(PLACE_LABEL),
                new JLabel(SPORT_TYPE_LABEL),
                new JLabel(START_DATE_LABEL),
                new JLabel(END_DATE_LABEL)};

        initBoxes();

        userDataPanels[0] = new JPanel(new GridLayout(1, 2));
        userDataPanels[0].add(userDataLabels[0]);
        userDataPanels[0].add(nameTextField);
        mainPanel.add(userDataPanels[0]);

        userDataPanels[1] = new JPanel(new GridLayout(1, 2));
        userDataPanels[1].add(userDataLabels[1]);
        userDataPanels[1].add(placeBox);
        mainPanel.add(userDataPanels[1]);

        userDataPanels[2] = new JPanel(new GridLayout(1, 2));
        userDataPanels[2].add(userDataLabels[2]);
        userDataPanels[2].add(sportTypeBox);
        mainPanel.add(userDataPanels[2]);

        userDataPanels[3] = new JPanel(new GridLayout(1, 6));
        userDataPanels[3].add(userDataLabels[3]);

        userDataPanels[3].add(dayBox);
        userDataPanels[3].add(monthBox);
        userDataPanels[3].add(yearBox);
        userDataPanels[3].add(hourBox);
        userDataPanels[3].add(minutesBox);
        mainPanel.add(userDataPanels[3]);

        userDataPanels[4] = new JPanel(new GridLayout(1, 6));
        userDataPanels[4].add(userDataLabels[4]);
        userDataPanels[4].add(dayBox2);
        userDataPanels[4].add(monthBox2);
        userDataPanels[4].add(yearBox2);
        userDataPanels[4].add(hourBox2);
        userDataPanels[4].add(minutesBox2);
        mainPanel.add(userDataPanels[4]);


        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        SportGuiManager.putMainFrameToCenter();
    }

    private void initBoxes() {
        ArrayList<String> hours = new ArrayList<>();
        ArrayList<String> minutes = new ArrayList<>();
        ArrayList<String> days = new ArrayList<>();
        ArrayList<String> years = new ArrayList<>();
        ArrayList<String> months = new ArrayList<>();
        ArrayList<String> sportTypes = DatabaseOperations.getAvailableSportTypes();
        sportTypeBox = new JComboBox();
        if (sportTypes != null) {
            for (String sportType : sportTypes) {
                sportTypeBox.addItem(sportType);
            }
        } else {
            return;
        }
        places = DatabaseOperations.getAvailablePlaces();
        placeBox = new JComboBox();
        if (places != null) {
            for (ArrayList<String> place : places) {
                placeBox.addItem(place.get(0) + " " + place.get(1));
            }
        } else {
            return;
        }
        for (int i = 0; i < 60; i++) {
            if (i < 24) {
                hours.add(((Integer) i).toString());
            }
            if (i > 0 && i < 32) {
                days.add(((Integer) i).toString());
            }
            if (i > 0 && i < 13) {
                months.add(((Integer) i).toString());
            }
            minutes.add(((Integer) i).toString());
            years.add(((Integer) (LocalDateTime.now().getYear() + i)).toString());
        }
        dayBox = new JComboBox();
        for (String day : days) {
            dayBox.addItem(day);
        }
        monthBox = new JComboBox();
        for (String month : months) {
            monthBox.addItem(month);
        }
        yearBox = new JComboBox();
        for (String year : years) {
            yearBox.addItem(year);
        }
        hourBox = new JComboBox();
        for (String hour : hours) {
            hourBox.addItem(hour);
        }
        minutesBox = new JComboBox();
        for (String minute : minutes) {
            minutesBox.addItem(minute);
        }
        dayBox2 = new JComboBox();
        for (String day : days) {
            dayBox2.addItem(day);
        }
        monthBox2 = new JComboBox();
        for (String month : months) {
            monthBox2.addItem(month);
        }
        yearBox2 = new JComboBox();
        for (String year : years) {
            yearBox2.addItem(year);
        }
        hourBox2 = new JComboBox();
        for (String hour : hours) {
            hourBox2.addItem(hour);
        }
        minutesBox2 = new JComboBox();
        for (String minute : minutes) {
            minutesBox2.addItem(minute);
        }
    }

    private void initButtonPanel() {
        JButton okButton = new JButton(OK_BUTTON_LABEL);
        JButton cancelButton = new JButton(CANCEL_BUTTON_LABEL);
        ButtonListener buttonListener = new ButtonListener();
        okButton.addActionListener(buttonListener);
        cancelButton.addActionListener(buttonListener);

        JPanel buttonPanel = new JPanel();

        buttonPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.weightx = 1;
        c.weighty = .25;
        c.insets = new Insets(5, 5, 5, 5);
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.fill = GridBagConstraints.BOTH;

        buttonPanel.add(okButton, c);
        buttonPanel.add(cancelButton, c);

        mainFrame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        mainFrame.pack();
    }

    private class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            final JButton button = (JButton) e.getSource();
            if (button.getText().equals(OK_BUTTON_LABEL)) {
                try {
                    if (nameTextField.getText().equals(EMPTY_STRING)) {
                        throw new IllegalArgumentException();
                    }
                    LocalDateTime startTime = LocalDateTime.of(Integer.parseInt(yearBox.getSelectedItem().toString()),
                            Integer.parseInt(monthBox.getSelectedItem().toString()),
                            Integer.parseInt(dayBox.getSelectedItem().toString()),
                            Integer.parseInt(hourBox.getSelectedItem().toString()),
                            Integer.parseInt(minutesBox.getSelectedItem().toString()));
                    LocalDateTime finishTime = LocalDateTime.of(Integer.parseInt(yearBox2.getSelectedItem().toString()),
                            Integer.parseInt(monthBox2.getSelectedItem().toString()),
                            Integer.parseInt(dayBox2.getSelectedItem().toString()),
                            Integer.parseInt(hourBox2.getSelectedItem().toString()),
                            Integer.parseInt(minutesBox2.getSelectedItem().toString()));
                    if (startTime.isAfter(finishTime)) {
                        throw new IllegalStateException();
                    }
                    if (startTime.isBefore(LocalDateTime.now())) {
                        throw new IllegalStateException();
                    }
                    DatabaseOperations.createCompetition(nameTextField.getText(),
                            places.get(placeBox.getSelectedIndex()),
                            Integer.parseInt(dayBox.getSelectedItem().toString()),
                            Integer.parseInt(monthBox.getSelectedItem().toString()),
                            Integer.parseInt(yearBox.getSelectedItem().toString()),
                            Integer.parseInt(hourBox.getSelectedItem().toString()),
                            Integer.parseInt(minutesBox.getSelectedItem().toString()),
                            Integer.parseInt(dayBox2.getSelectedItem().toString()),
                            Integer.parseInt(monthBox2.getSelectedItem().toString()),
                            Integer.parseInt(yearBox2.getSelectedItem().toString()),
                            Integer.parseInt(hourBox2.getSelectedItem().toString()),
                            Integer.parseInt(minutesBox2.getSelectedItem().toString()),
                            sportTypeBox.getSelectedItem().toString());
                    JOptionPane.showMessageDialog(CreateCompetitionView.this, "Competition created");
                    SportGuiManager.closeWindow();
                    mainFrame.add(new OrganizerView(mainFrame));
                    mainFrame.validate();
                }catch (IllegalArgumentException e1) {
                    JOptionPane.showMessageDialog(CreateCompetitionView.this, "You must fill out the entire form");
                    return;
                }catch (IllegalStateException e1) {
                    JOptionPane.showMessageDialog(CreateCompetitionView.this, "You've entered wrong date'");
                    return;
                }
            } else{
                SportGuiManager.closeWindow();
                mainFrame.add(new OrganizerView(mainFrame));
                mainFrame.validate();
            }
        }
    }
}
