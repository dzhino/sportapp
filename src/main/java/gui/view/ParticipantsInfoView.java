package gui.view;

import database.DatabaseOperations;
import gui.manager.SportGuiManager;
import gui.manager.UserData;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ParticipantsInfoView extends JPanel {

    private MainFrame mainFrame;
    private ArrayList<ArrayList<String>> giveBack;
    private ArrayList<ArrayList<String>> participants;

    private final int OPTIONS_COUNT;
    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    private static final String CANCEL_BUTTON = "Cancel";

    ParticipantsInfoView (MainFrame mainFrame, ArrayList<ArrayList<String>> giveBack, ArrayList<ArrayList<String>> participants) {
        this.mainFrame = mainFrame;
        this.giveBack = giveBack;
        this.participants = participants;
        OPTIONS_COUNT = participants.size() + 1;
        initMainPanel();
        SportGuiManager.putMainFrameToCenter();
        mainFrame.setVisible(true);
    }
    private void initMainPanel() {
        final JPanel mainPanel = new JPanel(new GridLayout (OPTIONS_COUNT, 1));
        final JPanel[] userDataPanels = new JPanel[OPTIONS_COUNT];

        ButtonListener buttonListener = new ButtonListener();

        for (int i = 0; i < participants.size(); i++) {
            userDataPanels[i] = new JPanel(new GridLayout(1, participants.get(i).size() - 1));

            for(int j = 1; j < participants.get(i).size(); j++)
            {
                JTextField stringField = new JTextField(participants.get(i).get(j), participants.get(i).get(j).length());
                stringField.setEnabled(false);
                userDataPanels[i].add(stringField);
            }
            mainPanel.add(userDataPanels[i]);
        }

        JButton cancelButton = new JButton(CANCEL_BUTTON);
        userDataPanels[OPTIONS_COUNT - 1] = new JPanel(new GridLayout(1, 1));
        userDataPanels[OPTIONS_COUNT - 1].add(cancelButton);

        mainPanel.add(userDataPanels[OPTIONS_COUNT - 1]);
        cancelButton.addActionListener(buttonListener);
        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        mainFrame.setResizable(false);
    }

    private class ButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton)e.getSource();
            if(button.getText().equals(CANCEL_BUTTON)) {
                if (UserData.getRoleId() == DatabaseOperations.getRoleID("COACH")) {
                    SportGuiManager.closeWindow();
                    mainFrame.add(new CoachSectionsView(mainFrame, giveBack));
                    mainFrame.validate();
                }
                if (UserData.getRoleId() == DatabaseOperations.getRoleID("ORGANIZER")) {
                    SportGuiManager.closeWindow();
                    mainFrame.add(new CompetitionsView (mainFrame, giveBack));
                    mainFrame.validate();
                }
            }
        }
    }
}
