package gui.view;

import database.DatabaseOperations;
import gui.manager.SportGuiManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DirectorView extends JPanel {
    private static final int FRAME_HEIGHT = 400;
    private static final int FRAME_WIDTH = 300;
    private static final int OPTIONS_COUNT = 2;

    private static final String COMPETITIONS_LIST_BUTTON = "Get competitions in your sport structure";
    private static final String SCHEDULE_BUTTON = "Schedule";

    private MainFrame mainFrame;

    public DirectorView(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        this.mainFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        JButton[] buttons = {new JButton(COMPETITIONS_LIST_BUTTON),
                new JButton(SCHEDULE_BUTTON)};

        GridBagConstraints[] gridBagConstraints = {new GridBagConstraints(),
                new GridBagConstraints()};

        ButtonListener buttonListener = new ButtonListener();

        for (int i = 0; i < buttons.length; i++) {
            gridBagConstraints[i].weightx = 0;
            gridBagConstraints[i].weighty = 0;
            gridBagConstraints[i].gridx = 0;
            gridBagConstraints[i].gridy = i;
            gridBagConstraints[i].gridheight = 1;
            gridBagConstraints[i].gridwidth = 1;
            add(buttons[i], gridBagConstraints[i]);
            buttons[i].addActionListener(buttonListener);
        }
        SportGuiManager.putMainFrameToCenter();
    }

    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            final JButton button = (JButton) e.getSource();
            switch(button.getText()) {
                case COMPETITIONS_LIST_BUTTON:
                    SportGuiManager.closeWindow();
                    mainFrame.add(new DirectorScheduleView(mainFrame, DatabaseOperations.getFutureCompetitionsByDirectorID()));
                    mainFrame.validate();
                    break;
                case SCHEDULE_BUTTON:
                    break;
            }
        }
    }
}
