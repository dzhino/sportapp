package gui.view;

import database.DatabaseOperations;
import gui.manager.SportGuiManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class StructuresListView extends JPanel {

    private MainFrame mainFrame;

    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    private static final String CANCEL_BUTTON_LABEL = "Cancel";

    StructuresListView(MainFrame mainFrame, ArrayList<String> structures) {
        this.mainFrame = mainFrame;

        JPanel mainPanel = new JPanel(new GridLayout(structures.size() + 1, 1));
        for (String place : structures){
            JPanel placePanel = new JPanel(new GridLayout(1, 1));
            JTextField textField = new JTextField(place, place.length());
            textField.setEnabled(false);
            placePanel.add(textField);
            mainPanel.add(placePanel);
        }
        JButton cancelButton = new JButton(CANCEL_BUTTON_LABEL);
        ButtonListener buttonListener = new ButtonListener();
        cancelButton.addActionListener(buttonListener);
        mainPanel.add(cancelButton);

        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        SportGuiManager.putMainFrameToCenter();
    }
    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton)e.getSource();
            if (button.getText().equals(CANCEL_BUTTON_LABEL)){
                SportGuiManager.closeWindow();
                mainFrame.add(new OrganizerView(mainFrame));
            }
        }
    }
}
