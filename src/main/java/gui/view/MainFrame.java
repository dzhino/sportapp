package gui.view;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {

    private static final int SCREEN_WIDTH;
    private static final int SCREEN_HEIGHT;

    static {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        SCREEN_WIDTH = dimension.width;
        SCREEN_HEIGHT = dimension.height;
    }

    public MainFrame() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("SportApp");
        setVisible(true);
    }
    @Override
    public void setSize (int width, int height) {
        setBounds((SCREEN_WIDTH - width) / 2, (SCREEN_HEIGHT - height) / 2, width, height);
    }
}
