package gui.view;

import database.DatabaseOperations;
import gui.manager.SportGuiManager;
import gui.manager.UserData;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class DirectorScheduleView extends JPanel {
    private MainFrame mainFrame;
    private ArrayList<ArrayList<String>> competitions;
    private final int ROWS_QUANTITY;
    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    DirectorScheduleView(MainFrame mainFrame, ArrayList<ArrayList<String>> competitions) {
        this.mainFrame = mainFrame;
        this.competitions = competitions;
        ROWS_QUANTITY = competitions.size() + 2;

        initMainPanel();
        SportGuiManager.putMainFrameToCenter();
        mainFrame.setVisible(true);
    }

    private void initMainPanel() {
        final JPanel mainPanel = new JPanel(new GridLayout(ROWS_QUANTITY, 1));
        final JPanel[] userDataPanels = new JPanel[ROWS_QUANTITY];

        userDataPanels[0] = new JPanel (new GridLayout(1, 3));
        userDataPanels[0].add(new JLabel("Competition name"));
        userDataPanels[0].add(new JLabel("Competition start date"));
        userDataPanels[0].add(new JLabel("Competition end date"));
        mainPanel.add(userDataPanels[0]);
        for (int i = 0; i < competitions.size(); i++) {
            userDataPanels[i + 1] = new JPanel(new GridLayout(1, competitions.get(i).size()));
            for (int j = 0; j < competitions.get(i).size(); j++) {
                JTextField stringField = new JTextField(competitions.get(i).get(j), competitions.get(i).get(j).length());
                stringField.setEnabled(false);
                userDataPanels[i + 1].add(stringField);
            }
            mainPanel.add(userDataPanels[i + 1]);
        }

        JButton cancelButton = new JButton("Cancel");
        userDataPanels[ROWS_QUANTITY - 1] = new JPanel(new GridLayout(1, 1));
        userDataPanels[ROWS_QUANTITY - 1].add(cancelButton);

        mainPanel.add(userDataPanels[ROWS_QUANTITY - 1]);
        cancelButton.addActionListener(e -> {
            SportGuiManager.closeWindow();
            mainFrame.add(new DirectorView(mainFrame));
            mainFrame.validate();
        });
        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        mainFrame.setResizable(false);
    }
}
