package gui.view;

import database.DatabaseOperations;
import gui.manager.SportGuiManager;
import gui.manager.UserData;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

class CompetitionsView extends JPanel {

    private MainFrame mainFrame;
    private ArrayList<ArrayList<String>> competitions;
    private ArrayList<JButton> scheduleButtons = new ArrayList<>();
    private ArrayList<JButton> joinButtons = new ArrayList<>();
    private ArrayList<JButton> winnersButtons = new ArrayList<>();
    private ArrayList<JButton> participantsButtons = new ArrayList<>();

    private final int ROWS_QUANTITY;
    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    private static final String CANCEL_BUTTON = "Cancel";
    private static final String SCHEDULE_BUTTON = "Schedule";
    private static final String JOIN_BUTTON = "Join";
    private static final String WINNERS_BUTTON = "Winners";
    private static final String PARTICIPANT_BUTTON = "Participants";

    CompetitionsView (MainFrame mainFrame, ArrayList<ArrayList<String>> competitions){
        this.mainFrame = mainFrame;
        this.competitions = competitions;
        ROWS_QUANTITY = this.competitions.size() + 1;

        initMainPanel();
        SportGuiManager.putMainFrameToCenter();
        mainFrame.setVisible(true);
    }
    private void initMainPanel() {
        final JPanel mainPanel = new JPanel(new GridLayout(ROWS_QUANTITY, 1));
        final JPanel[] userDataPanels = new JPanel[ROWS_QUANTITY];

        ButtonListener buttonListener = new ButtonListener();

        for (int i = 0; i < ROWS_QUANTITY - 1; i++) {
            userDataPanels[i] = new JPanel(new GridLayout(1, competitions.get(i).size() + 1));

            for(int j = 4; j < competitions.get(i).size(); j++)
            {
                JTextField stringField = new JTextField(competitions.get(i).get(j), competitions.get(i).get(j).length());
                stringField.setEnabled(false);
                userDataPanels[i].add(stringField);
            }

            scheduleButtons.add(new JButton(SCHEDULE_BUTTON));
            if (UserData.getRoleId() == DatabaseOperations.getRoleID("SPORTSMEN") && SportGuiManager.getFutureView().equals(SportGuiManager.getFutureCompetitionView())) {
                joinButtons.add(new JButton(JOIN_BUTTON));
                joinButtons.get(i).addActionListener(buttonListener);
                userDataPanels[i].add(joinButtons.get(i));
            } else if (UserData.getRoleId() == DatabaseOperations.getRoleID("ORGANIZER") && SportGuiManager.getFutureView().equals(SportGuiManager.getFutureCompetitionView())) {
                participantsButtons.add(new JButton(PARTICIPANT_BUTTON));
                participantsButtons.get(i).addActionListener(buttonListener);
                userDataPanels[i].add(participantsButtons.get(i));
            } else if (SportGuiManager.getFutureView().equals(SportGuiManager.getLastCompetitionView())) {
                winnersButtons.add(new JButton(WINNERS_BUTTON));
                winnersButtons.get(i).addActionListener(buttonListener);
                userDataPanels[i].add(winnersButtons.get(i));
            }
            scheduleButtons.get(i).addActionListener(buttonListener);

            userDataPanels[i].add(scheduleButtons.get(i));

            mainPanel.add(userDataPanels[i]);
        }

        JButton cancelButton = new JButton(CANCEL_BUTTON);
        userDataPanels[ROWS_QUANTITY - 1] = new JPanel(new GridLayout(1, 1));
        userDataPanels[ROWS_QUANTITY - 1].add(cancelButton);

        mainPanel.add(userDataPanels[ROWS_QUANTITY - 1]);
        cancelButton.addActionListener(buttonListener);
        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        mainFrame.setResizable(false);
    }

    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton)e.getSource();
            if(button.getText().equals(CANCEL_BUTTON)) {
                SportGuiManager.closeWindow();
                mainFrame.add(new ChooseSportTypeView(mainFrame));
                mainFrame.validate();
            }
            for (int i = 0; i < scheduleButtons.size(); i++) {
                if (scheduleButtons.get(i).equals(button)) {
                    SportGuiManager.closeWindow();
                    SportGuiManager.setLastView(SportGuiManager.getSectionsView());
                    mainFrame.add(new CompetitionsScheduleView(mainFrame, competitions, i));
                    mainFrame.validate();
                }
            }
            for (int i = 0; i < joinButtons.size(); i++) {
                if (joinButtons.get(i).equals(button)) {
                    SportGuiManager.closeWindow();
                    DatabaseOperations.joinCompetition(competitions.get(i).get(0));
                    JOptionPane.showMessageDialog(CompetitionsView.this, "You joined to competition");
                    mainFrame.add(new SportsmenView(mainFrame));
                    mainFrame.validate();
                }
            }
            for (int i = 0; i < participantsButtons.size(); i++) {
                if (participantsButtons.get(i).equals(button)) {
                    SportGuiManager.closeWindow();
                    SportGuiManager.setLastView(SportGuiManager.getCompetitionsView());
                    mainFrame.add(new ParticipantsInfoView(mainFrame, competitions, DatabaseOperations.getCompetitionParticipants(competitions.get(i).get(0))));
                    mainFrame.validate();
                }
            }
        }
    }
}