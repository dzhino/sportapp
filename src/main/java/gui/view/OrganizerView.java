package gui.view;

import gui.manager.SportGuiManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OrganizerView extends JPanel {
    private static final int FRAME_HEIGHT = 400;
    private static final int FRAME_WIDTH = 300;
    private static final int OPTIONS_COUNT = 5;

    private static final String SPORT_STRUCTURES_LIST_BUTTON = "Get sport structures list";
    private static final String LAST_COMPETITIONS_LIST_BUTTON = "Get last competitions list";
    private static final String FUTURE_COMPETITIONS_LIST_BUTTON = "Get future competitions list";
    private static final String PARTICIPANTS_LIST_BUTTON = "Get competition participants list";
    private static final String CREATE_COMPETITION_BUTTON = "Create new competition";

    private MainFrame mainFrame;

    public OrganizerView(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        this.mainFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        JButton[] buttons = {new JButton(SPORT_STRUCTURES_LIST_BUTTON),
                new JButton(FUTURE_COMPETITIONS_LIST_BUTTON),
                new JButton(LAST_COMPETITIONS_LIST_BUTTON),
                new JButton(CREATE_COMPETITION_BUTTON)};

        GridBagConstraints[] gridBagConstraints = {new GridBagConstraints(),
                new GridBagConstraints(),
                new GridBagConstraints(),
                new GridBagConstraints(),
                new GridBagConstraints(),
                new GridBagConstraints()};

        ButtonListener buttonListener = new ButtonListener();

        for (int i = 0; i < buttons.length; i++) {
            gridBagConstraints[i].weightx = 0;
            gridBagConstraints[i].weighty = 0;
            gridBagConstraints[i].gridx = 0;
            gridBagConstraints[i].gridy = i;
            gridBagConstraints[i].gridheight = 1;
            gridBagConstraints[i].gridwidth = 1;
            add(buttons[i], gridBagConstraints[i]);
            buttons[i].addActionListener(buttonListener);
        }
        SportGuiManager.putMainFrameToCenter();
    }

    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            final JButton button = (JButton) e.getSource();
            switch(button.getText()) {
                case SPORT_STRUCTURES_LIST_BUTTON:
                    SportGuiManager.closeWindow();
                    mainFrame.add(new ChooseStructureTypeView(mainFrame));
                    mainFrame.validate();
                    break;
                case FUTURE_COMPETITIONS_LIST_BUTTON:
                    SportGuiManager.setFutureView(SportGuiManager.getFutureCompetitionView());
                    SportGuiManager.closeWindow();
                    mainFrame.add(new ChooseSportTypeView(mainFrame));
                    mainFrame.validate();
                    break;
                case LAST_COMPETITIONS_LIST_BUTTON:
                    SportGuiManager.setFutureView(SportGuiManager.getLastCompetitionView());
                    SportGuiManager.closeWindow();
                    mainFrame.add(new ChooseSportTypeView(mainFrame));
                    mainFrame.validate();
                    break;
                case CREATE_COMPETITION_BUTTON:
                    SportGuiManager.closeWindow();
                    mainFrame.add(new CreateCompetitionView(mainFrame));
                    mainFrame.validate();
                    break;
            }
        }
    }
}
