package gui.view;

import database.DatabaseOperations;
import gui.manager.SportGuiManager;
import gui.manager.UserData;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ChooseSportTypeView extends JPanel{

    private MainFrame mainFrame;
    private static final String CHOOSE_SPORT_LABEL = "Choose sport's type";
    private static final String OK_BUTTON = "OK";
    private static final String CANCEL_BUTTON = "Cancel";

    private JComboBox sportTypesBox = new JComboBox();

    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    ChooseSportTypeView(MainFrame mainFrame){
        this.mainFrame = mainFrame;
        JPanel mainPanel = new JPanel(new GridLayout(2,1));
        JPanel[] insidePanels = new JPanel[2];

        ArrayList<String> sportTypes = DatabaseOperations.getAvailableSportTypes();
        JLabel chooseSportLabel = new JLabel(CHOOSE_SPORT_LABEL);

        for (String role : sportTypes){
            sportTypesBox.addItem(role);
        }
        ButtonListener buttonListener = new ButtonListener();
        JButton okButton = new JButton(OK_BUTTON);
        JButton cancelButton = new JButton(CANCEL_BUTTON);
        okButton.addActionListener(buttonListener);
        cancelButton.addActionListener(buttonListener);

        insidePanels[0] = new JPanel(new GridLayout(1,2));
        insidePanels[0].add(chooseSportLabel);
        insidePanels[0].add(sportTypesBox);
        insidePanels[1] = new JPanel(new GridLayout(1, 2));
        insidePanels[1].add(cancelButton);
        insidePanels[1].add(okButton);

        mainPanel.add(insidePanels[0]);
        mainPanel.add(insidePanels[1]);

        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        SportGuiManager.putMainFrameToCenter();
        mainFrame.setVisible(true);
    }
    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton)e.getSource();
            if (button.getText().equals(OK_BUTTON)){
                SportGuiManager.closeWindow();
                if(SportGuiManager.getFutureView().equals(SportGuiManager.getSectionsView())) {
                    mainFrame.add(new SectionsView(mainFrame, DatabaseOperations.getSectionsList(sportTypesBox.getSelectedItem().toString())));

                } else if (SportGuiManager.getFutureView().equals(SportGuiManager.getFutureCompetitionView())) {
                    if (UserData.getRoleId() == DatabaseOperations.getRoleID("ORGANIZER"))
                    {
                        mainFrame.add(new CompetitionsView(mainFrame, DatabaseOperations.getFutureOrganizerCompetitions(sportTypesBox.getSelectedItem().toString())));
                    } else {
                        mainFrame.add(new CompetitionsView(mainFrame, DatabaseOperations.getFutureCompetitions(sportTypesBox.getSelectedItem().toString())));
                    }
                } else if (SportGuiManager.getFutureView().equals(SportGuiManager.getLastCompetitionView())) {
                    if (UserData.getRoleId() == DatabaseOperations.getRoleID("ORGANIZER")) {
                        mainFrame.add(new CompetitionsView(mainFrame, DatabaseOperations.getLastOrganizerCompetitions(sportTypesBox.getSelectedItem().toString())));
                    } else {
                        mainFrame.add(new CompetitionsView(mainFrame, DatabaseOperations.getLastCompetitions(sportTypesBox.getSelectedItem().toString())));
                    }
                }
                mainFrame.validate();
            } else {
                SportGuiManager.closeWindow();
                SportGuiManager.chooseNextView();
                validate();
            }
        }
    }
}
