package gui.view;

import database.DatabaseOperations;
import database.Roles;
import gui.manager.SportGuiManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TimeOptionsView extends JPanel {
    MainFrame mainFrame;
    private final ArrayList <String> chosenDays;
    private final String chosenSportType;
    private final ArrayList<String> chosenSportStructure;

    private final int OPTIONS_COUNT;
    private static final int TEXT_FIELD_COLUMNS_COUNT = 10;
    private static final int TOP_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    private static final String OK_BUTTON_LABEL = "OK";
    private static final String CANCEL_BUTTON_LABEL = "Cancel";
    private static final String DAYS_LABEL = "Days";
    private static final String START_TIME_LABEL = "Start time";
    private static final String FINISH_TIME_LABEL = "Finish time";
    private static final String EMPTY_STRING = "";

    private ArrayList<JTextField> startComponents;
    private ArrayList<JTextField> finishComponents;

    TimeOptionsView (MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        chosenSportType = DayOptionsView.getChosenSportType();
        chosenDays = DayOptionsView.getChosenDays();
        chosenSportStructure = DayOptionsView.getChosenSportStructure();

        OPTIONS_COUNT = chosenDays.size();
        startComponents = new ArrayList<>();
        finishComponents = new ArrayList<>();
        initMainPanel();
        initButtonPanel();
        mainFrame.pack();

        SportGuiManager.putMainFrameToCenter();

        mainFrame.setResizable(false);
        mainFrame.setVisible(true);
    }
    private void initMainPanel() {
        final JPanel mainPanel = new JPanel(new GridLayout(OPTIONS_COUNT + 1, 1));
        final JPanel[] timePanels = new JPanel[OPTIONS_COUNT + 1];
        timePanels[0] = new JPanel(new GridLayout(1, 3));
        timePanels[0].add(new JLabel(DAYS_LABEL));
        timePanels[0].add(new JLabel(START_TIME_LABEL));
        timePanels[0].add(new JLabel(FINISH_TIME_LABEL));
        mainPanel.add(timePanels[0]);
        for (int i = 0; i < OPTIONS_COUNT; i++) {
            final JTextField startTimeTextField = new JTextField(TEXT_FIELD_COLUMNS_COUNT);
            final JTextField finishTimeTextField = new JTextField(TEXT_FIELD_COLUMNS_COUNT);
            startComponents.add (startTimeTextField);
            finishComponents.add (finishTimeTextField);

            timePanels[i + 1] = new JPanel(new GridLayout(1, 3));
            timePanels[i + 1].add(new JLabel(chosenDays.get(i)));
            timePanels[i + 1].add(startTimeTextField);
            timePanels[i + 1].add(finishTimeTextField);

            mainPanel.add(timePanels[i + 1]);
        }

        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
    }

    private void initButtonPanel() {
        JButton okButton = new JButton(OK_BUTTON_LABEL);
        JButton cancelButton = new JButton(CANCEL_BUTTON_LABEL);
        ButtonListener buttonListener = new ButtonListener();
        okButton.addActionListener(buttonListener);
        cancelButton.addActionListener(buttonListener);

        JPanel buttonPanel = new JPanel();

        buttonPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.weightx = 1;
        c.weighty = .25;
        c.insets = new Insets(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER);
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.fill = GridBagConstraints.BOTH;

        buttonPanel.add(okButton, c);
        buttonPanel.add(cancelButton, c);

        mainFrame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        mainFrame.pack();
    }

    private class ButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton) e.getSource();
            if (button.getText().equals(OK_BUTTON_LABEL)) {
                try {
                    for (int i = 0; i < startComponents.size(); i++) {
                        if (startComponents.get(i).getText().equals(EMPTY_STRING) || finishComponents.get(i).getText().equals(EMPTY_STRING)) {
                            throw new IllegalArgumentException();
                        }
                    }
                    ArrayList<String>startTime = new ArrayList<>();
                    ArrayList<String>finishTime = new ArrayList<>();
                    for (int i = 0; i < startComponents.size(); i++) {
                        startTime.add(startComponents.get(i).getText());
                        finishTime.add(finishComponents.get(i).getText());
                    }

                    DatabaseOperations.createSection(chosenDays, startTime, finishTime, chosenSportType, chosenSportStructure);
                    JOptionPane.showMessageDialog(TimeOptionsView.this, "Section was created");
                    SportGuiManager.closeWindow();
                    mainFrame.add (new CoachView(mainFrame));
                } catch (IllegalArgumentException exp) {
                    JOptionPane.showMessageDialog(TimeOptionsView.this, "You must fill out the entire form");
                    return;
                }
            } else {
                SportGuiManager.closeWindow();
                chosenDays.clear();
                mainFrame.add(new DayOptionsView(mainFrame));
                mainFrame.validate();
            }
        }
    }
}
