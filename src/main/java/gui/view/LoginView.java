package gui.view;

import database.DatabaseOperations;
import gui.manager.SportGuiManager;
import gui.manager.UserData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginView extends JPanel {
    private JTextField login;
    private MainFrame mainFrame;

    private static final String LOGIN_LABEL = "EMail";
    private static final String OK_BUTTON = "OK";
    private static final String CANCEL_BUTTON = "Cancel";
    private static final String EMPTY_STRING = "";

    private static final int TEXT_FIELD_SIZE = 20;
    private static final int FRAME_HEIGHT = 300;
    private static final int FRAME_WIDTH = 400;


    LoginView (MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        mainFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        GridBagLayout layout = new GridBagLayout();
        setLayout(layout);
        GridBagConstraints constraintsLabel = new GridBagConstraints();
        constraintsLabel.weightx = 0;
        constraintsLabel.weighty = 0;
        constraintsLabel.gridx = 0;
        constraintsLabel.gridy = 0;
        constraintsLabel.gridheight = 1;
        constraintsLabel.gridwidth = 1;
        add(new JLabel(LOGIN_LABEL), constraintsLabel);

        GridBagConstraints constraintsLogin = new GridBagConstraints();
        constraintsLogin.weightx = 0;
        constraintsLogin.weighty = 0;
        constraintsLogin.gridx = 1;
        constraintsLogin.gridy = 0;
        constraintsLogin.gridheight = 1;
        constraintsLogin.gridwidth = 1;
        login = new JTextField(TEXT_FIELD_SIZE);
        add(login, constraintsLogin);

        GridBagConstraints constraintsOkButton = new GridBagConstraints();
        constraintsOkButton.weightx = 0;
        constraintsOkButton.weighty = 0;
        constraintsOkButton.gridx = 1;
        constraintsOkButton.gridy = 1;
        constraintsOkButton.gridheight = 1;
        constraintsOkButton.gridwidth = 1;
        JButton okButton = new JButton(OK_BUTTON);
        add(okButton, constraintsOkButton);

        GridBagConstraints constraintsCancelButton = new GridBagConstraints();
        constraintsCancelButton.weightx = 0;
        constraintsCancelButton.weighty = 0;
        constraintsCancelButton.gridx = 0;
        constraintsCancelButton.gridy = 1;
        constraintsCancelButton.gridheight = 1;
        constraintsCancelButton.gridwidth = 1;
        JButton cancelButton = new JButton(CANCEL_BUTTON);
        add(cancelButton, constraintsCancelButton);

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String value = login.getText();
                try {
                    if (value.equals(EMPTY_STRING)) {
                        throw new IllegalStateException();
                    } else {
                        if (DatabaseOperations.isRegistered(value)) {
                            DatabaseOperations.getUserData(value);
                            SportGuiManager.closeWindow();
                            SportGuiManager.chooseNextView();
                            mainFrame.validate();
                        } else throw new IllegalArgumentException();
                    }
                } catch (IllegalStateException exception) {
                    JOptionPane.showMessageDialog(LoginView.this, "You must fill out the entire form");
                } catch (IllegalArgumentException exception) {
                    JOptionPane.showMessageDialog(LoginView.this, "You aren't registered or entered wrong email");
                }
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SportGuiManager.closeWindow();
                mainFrame.add(new StartView(mainFrame));
                mainFrame.validate();
            }
        });
        SportGuiManager.putMainFrameToCenter();
    }
}
