package gui.view;

import database.DatabaseOperations;
import gui.manager.SportGuiManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ScheduleView extends JPanel {
    private MainFrame mainFrame;
    private ArrayList<ArrayList<String>> sections;
    private ArrayList<String> section;
    private ArrayList<ArrayList<String>> schedule;

    private static final String DAYS_LABEL = "Days";
    private static final String START_TIME_LABEL = "Start time";
    private static final String FINISH_TIME_LABEL = "Finish time";
    private static final String CANCEL_BUTTON = "Cancel";

    private final int OPTIONS_COUNT;
    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    ScheduleView(MainFrame mainFrame, ArrayList<ArrayList<String>> sections, int index) {
        this.mainFrame = mainFrame;
        this.sections = sections;
        this.section = sections.get(index);

        schedule = DatabaseOperations.getSchedule(section.get(0));
        OPTIONS_COUNT = schedule.size() + 1;

        initMainPanel();
        mainFrame.pack();

        SportGuiManager.putMainFrameToCenter();

        mainFrame.setResizable(false);
        mainFrame.setVisible(true);
    }
    private void initMainPanel() {
        final JPanel mainPanel = new JPanel(new GridLayout (OPTIONS_COUNT, 1));
        final JPanel[] userDataPanels = new JPanel[OPTIONS_COUNT];

        ButtonListener buttonListener = new ButtonListener();

        for (int i = 0; i < OPTIONS_COUNT - 1; i++) {
            userDataPanels[i] = new JPanel(new GridLayout(1, schedule.get(i).size()));

            for (int j = 0; j < schedule.get(i).size(); j++)
            {
                JTextField stringField = new JTextField(schedule.get(i).get(j), schedule.get(i).get(j).length());
                stringField.setEnabled(false);
                userDataPanels[i].add(stringField);
            }
            mainPanel.add(userDataPanels[i]);
        }

        JButton cancelButton = new JButton(CANCEL_BUTTON);
        userDataPanels[OPTIONS_COUNT - 1] = new JPanel(new GridLayout(1, 1));
        userDataPanels[OPTIONS_COUNT - 1].add(cancelButton);

        mainPanel.add(userDataPanels[OPTIONS_COUNT - 1]);
        cancelButton.addActionListener(buttonListener);
        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        Dimension frameSize = mainFrame.getSize();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        mainFrame.setBounds((screenSize.width - frameSize.width)/2, (screenSize.height - frameSize.height)/2, frameSize.width, frameSize.height);
        mainFrame.setResizable(false);
    }

    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(SportGuiManager.getLastView().equals(SportGuiManager.getSectionsView())) {
                SportGuiManager.closeWindow();
                mainFrame.add(new SectionsView(mainFrame, sections));
                mainFrame.validate();
            } else if (SportGuiManager.getLastView().equals(SportGuiManager.getSportsmenSectionsView())){
                SportGuiManager.closeWindow();
                mainFrame.add(new SportsmenSectionsView(mainFrame,sections));
                mainFrame.validate();
            } else {
                SportGuiManager.closeWindow();
                mainFrame.add(new CoachSectionsView(mainFrame,sections));
                mainFrame.validate();
            }
        }
    }
}
