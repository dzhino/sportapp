package gui.view;

import gui.manager.SportGuiManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class CompetitionsScheduleView extends JPanel {
    MainFrame mainFrame;
    CompetitionsScheduleView(MainFrame mainFrame, ArrayList<ArrayList<String>> competitions, int index) {
        JPanel mainPanel = new JPanel(new GridLayout(3, 1));
        this.mainFrame = mainFrame;
        JPanel panel1 = new JPanel(new GridLayout(1, 2));
        JTextField textField1 = new JTextField(competitions.get(index).get(2), competitions.get(index).get(3).length());
        textField1.setEnabled(false);
        panel1.add(new JLabel("Start competitions date"));
        panel1.add(textField1);

        JPanel panel2 = new JPanel(new GridLayout(1, 2));
        JTextField textField2 = new JTextField(competitions.get(index).get(3), competitions.get(index).get(4).length());
        textField2.setEnabled(false);
        panel2.add(new JLabel("End competitions date"));
        panel2.add(textField2);

        mainPanel.add (panel1);
        mainPanel.add(panel2);

        JButton cancelButton = new JButton("Cancel");
        mainPanel.add(cancelButton);

        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        mainFrame.setResizable(false);
        SportGuiManager.putMainFrameToCenter();

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SportGuiManager.closeWindow();
                mainFrame.add(new CompetitionsView(mainFrame,competitions));
            }
        });
    }
}
