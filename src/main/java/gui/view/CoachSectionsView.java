package gui.view;

import database.DatabaseOperations;
import gui.manager.SportGuiManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class CoachSectionsView extends JPanel {

    private MainFrame mainFrame;
    private ArrayList<ArrayList<String>> sections;
    private ArrayList<JButton> scheduleButtons = new ArrayList<>();
    private ArrayList<JButton> coachButtons = new ArrayList<>();
    private ArrayList<JButton> deleteButtons = new ArrayList<>();

    private final int OPTIONS_COUNT;
    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    private static final String CANCEL_BUTTON = "Cancel";
    private static final String PARTICIPANTS_BUTTON = "Participants contacts";
    private static final String SCHEDULE_BUTTON = "Schedule";
    private static final String DELETE_BUTTON = "Delete";

    CoachSectionsView(MainFrame mainFrame, ArrayList<ArrayList<String>> sections){
        this.mainFrame = mainFrame;
        this.sections = sections;
        OPTIONS_COUNT = sections.size() + 1;
        initMainPanel();
        SportGuiManager.putMainFrameToCenter();
        mainFrame.setVisible(true);
    }
    private void initMainPanel() {
        final JPanel mainPanel = new JPanel(new GridLayout(OPTIONS_COUNT, 1));
        final JPanel[] userDataPanels = new JPanel[OPTIONS_COUNT];

        ButtonListener buttonListener = new ButtonListener();

        for (int i = 0; i < sections.size(); i++) {
            userDataPanels[i] = new JPanel(new GridLayout(1, sections.get(i).size() + 1));

            JTextField stringField = new JTextField(sections.get(i).get(6), sections.get(i).get(6).length());
            stringField.setEnabled(false);
            userDataPanels[i].add(stringField);
            JTextField stringField2 = new JTextField(sections.get(i).get(7), sections.get(i).get(7).length());
            stringField2.setEnabled(false);
            userDataPanels[i].add(stringField2);


            coachButtons.add(new JButton(PARTICIPANTS_BUTTON));
            scheduleButtons.add(new JButton(SCHEDULE_BUTTON));
            deleteButtons.add(new JButton(DELETE_BUTTON));

            coachButtons.get(i).addActionListener(buttonListener);
            scheduleButtons.get(i).addActionListener(buttonListener);
            deleteButtons.get(i).addActionListener(buttonListener);

            userDataPanels[i].add(coachButtons.get(i));
            userDataPanels[i].add(scheduleButtons.get(i));
            userDataPanels[i].add(deleteButtons.get(i));

            mainPanel.add(userDataPanels[i]);
        }

        JButton cancelButton = new JButton(CANCEL_BUTTON);
        userDataPanels[OPTIONS_COUNT - 1] = new JPanel(new GridLayout(1, 1));
        userDataPanels[OPTIONS_COUNT - 1].add(cancelButton);

        mainPanel.add(userDataPanels[OPTIONS_COUNT - 1]);
        cancelButton.addActionListener(buttonListener);
        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        Dimension frameSize = mainFrame.getSize();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        mainFrame.setBounds((screenSize.width - frameSize.width)/2, (screenSize.height - frameSize.height)/2, frameSize.width, frameSize.height);
        mainFrame.setResizable(false);
    }

    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton)e.getSource();
            if(button.getText().equals(CANCEL_BUTTON)) {
                SportGuiManager.closeWindow();
                mainFrame.add(new CoachView(mainFrame));
                mainFrame.validate();
            }
            for (int i = 0; i < coachButtons.size(); i++) {
                if (coachButtons.get(i).equals(button)) {
                    SportGuiManager.closeWindow();
                    SportGuiManager.setLastView(SportGuiManager.getCoachSectionsView());
                    mainFrame.add (new ParticipantsInfoView (mainFrame, sections, DatabaseOperations.getParticipants(sections.get(i).get(0))));
                    mainFrame.validate();
                }
            }
            for (int i = 0; i < scheduleButtons.size(); i++) {
                if (scheduleButtons.get(i).equals(button)) {
                    SportGuiManager.closeWindow();
                    SportGuiManager.setLastView(SportGuiManager.getCoachSectionsView());
                    mainFrame.add(new ScheduleView (mainFrame, sections, i));
                    mainFrame.validate();
                }
            }
            for (int i = 0; i < deleteButtons.size(); i++) {
                if (deleteButtons.get(i).equals(button)) {
                    DatabaseOperations.dropTables(sections.get(i).get(0));
                    SportGuiManager.closeWindow();
                    JOptionPane.showMessageDialog(CoachSectionsView.this, "You deleted section");
                    mainFrame.add(new CoachView(mainFrame));
                    mainFrame.validate();
                }
            }
        }
    }
}
