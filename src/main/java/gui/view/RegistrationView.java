package gui.view;

import database.DatabaseOperations;
import database.Roles;
import gui.manager.SportGuiManager;
import gui.manager.UserData;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

class RegistrationView extends JPanel {

    private static final String REGISTRATION_VIEW_TITLE = "Registration";
    private static final String EMPTY_STRING = "";

    private static final String FIRST_NAME_LABEL = "First Name";
    private static final String SECOND_NAME_LABEL = "Second Name";
    private static final String THIRD_NAME_LABEL = "Third Name";
    private static final String PHONE_NUMBER_LABEL = "Phone number";
    private static final String ROLE_LABEL = "Role";
    private static final String EMAIL_LABEL = "Email";
    private static final String OK_BUTTON_LABEL = "OK";
    private static final String CANCEL_BUTTON_LABEL = "Cancel";


    private static final int OPTIONS_COUNT = 6;
    private static final int ROLES_QUANTITY = 4;
    private static final int TEXT_FIELD_COLUMNS_COUNT = 15;
    private static final int FRAME_WIDTH = 600;
    private static final int FRAME_HEIGHT = 400;
    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;
    private JComboBox jComboBox;


    private Map<Integer, JTextField> components;
    private MainFrame mainFrame;

    RegistrationView(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        this.components = new HashMap<>();

        mainFrame.setTitle(REGISTRATION_VIEW_TITLE);
        mainFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        initMainPanel();
        initButtonPanel();
        mainFrame.pack();

        mainFrame.setResizable(false);
        mainFrame.setVisible(true);
    }

    private void initMainPanel() {
        final JPanel mainPanel = new JPanel(new GridLayout(OPTIONS_COUNT, 1));
        final JPanel[] userDataPanels = new JPanel[OPTIONS_COUNT];
        final JLabel[] userDataLabels = new JLabel[]{new JLabel(FIRST_NAME_LABEL),
                new JLabel(SECOND_NAME_LABEL),
                new JLabel(THIRD_NAME_LABEL),
                new JLabel(EMAIL_LABEL),
                new JLabel(PHONE_NUMBER_LABEL),
                new JLabel(ROLE_LABEL)};

        for (int i = 0; i < userDataPanels.length - 1; i++) {
            final JTextField textField = new JTextField(TEXT_FIELD_COLUMNS_COUNT);
            components.put(i, textField);

            userDataPanels[i] = new JPanel(new GridLayout(1, 2));
            userDataPanels[i].add(userDataLabels[i]);
            userDataPanels[i].add(textField);

            mainPanel.add(userDataPanels[i]);
        }


        ArrayList<String>roles = DatabaseOperations.getAvailableRoles();
        jComboBox = new JComboBox();
        if (roles!= null) {
            for (String role : roles) {
                jComboBox.addItem(role);
            }
        } else {
            return;
        }
        userDataPanels[OPTIONS_COUNT - 1] = new JPanel(new GridLayout(1, 2));
        userDataPanels[OPTIONS_COUNT - 1].add(userDataLabels[OPTIONS_COUNT - 1]);
        userDataPanels[OPTIONS_COUNT - 1].add(jComboBox);

        mainPanel.add(userDataPanels[OPTIONS_COUNT - 1]);

        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        SportGuiManager.putMainFrameToCenter();
    }

    private void initButtonPanel() {
        JButton okButton = new JButton(OK_BUTTON_LABEL);
        JButton cancelButton = new JButton(CANCEL_BUTTON_LABEL);
        ButtonListener buttonListener = new ButtonListener();
        okButton.addActionListener(buttonListener);
        cancelButton.addActionListener(buttonListener);

        JPanel buttonPanel = new JPanel();

        buttonPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.weightx = 1;
        c.weighty = .25;
        c.insets = new Insets(5, 5, 5, 5);
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.fill = GridBagConstraints.BOTH;

        buttonPanel.add(okButton, c);
        buttonPanel.add(cancelButton, c);

        mainFrame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        mainFrame.pack();
    }

    private class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            final JButton button = (JButton) e.getSource();
            ArrayList <String> textFields = new ArrayList<>();
            if (button.getText().equals(OK_BUTTON_LABEL)) {
                for (Map.Entry<Integer, JTextField> entry : components.entrySet()) {
                    final String value = entry.getValue().getText();
                    textFields.add(value);
                    try {
                        if (value.equals(EMPTY_STRING)) {
                            throw new IllegalStateException();
                        }
                    } catch (IllegalStateException e1) {
                        JOptionPane.showMessageDialog(RegistrationView.this, "You must fill out the entire form");
                        return;
                    }
                }
                textFields.add(jComboBox.getSelectedItem().toString());
                try {
                    if (!DatabaseOperations.isRegistered(textFields.get(3))){
                        DatabaseOperations.Registration(textFields.get(0),
                                textFields.get(1),
                                textFields.get(2),
                                textFields.get(3),
                                textFields.get(4),
                                DatabaseOperations.getRoleID(textFields.get(5)));
                        if (DatabaseOperations.isRegistered(textFields.get(3))) {
                            DatabaseOperations.getUserData(textFields.get(3));
                            mainFrame.setTitle("SportApp");
                            SportGuiManager.closeWindow();
                            SportGuiManager.chooseNextView();
                            mainFrame.validate();
                        }
                    } else throw new IllegalArgumentException();
                } catch (IllegalArgumentException exp){
                    JOptionPane.showMessageDialog(RegistrationView.this, "Such email is already registered");
                }
            } else {
                SportGuiManager.closeWindow();
                mainFrame.add(new StartView(mainFrame));
                mainFrame.validate();
            }
        }
    }
}
