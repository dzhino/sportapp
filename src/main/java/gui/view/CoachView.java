package gui.view;


import database.DatabaseOperations;
import gui.manager.SportGuiManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CoachView extends JPanel {
    private static final int FRAME_HEIGHT = 400;
    private static final int FRAME_WIDTH = 300;
    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    private static final String MY_SECTIONS_BUTTON = "My sections";
    private static final String LAST_COMPETITIONS_BUTTON = "Get last competitions list";
    private static final String FUTURE_COMPETITIONS_BUTTON = "Get future competitions list";
    private static final String CREATE_SECTION_BUTTON = "Create new section";
    private static final String CREATE_CLUB_BUTTON = "Create new sport club";

    private MainFrame mainFrame;

    public CoachView(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        this.mainFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        JButton[] buttons = {new JButton(MY_SECTIONS_BUTTON),
                new JButton(LAST_COMPETITIONS_BUTTON),
                new JButton(FUTURE_COMPETITIONS_BUTTON),
                new JButton(CREATE_SECTION_BUTTON),
                new JButton(CREATE_CLUB_BUTTON)};

        ButtonListener buttonListener = new ButtonListener();
        JPanel mainPanel = new JPanel(new GridLayout(buttons.length, 1));
        for (JButton button : buttons) {
            mainPanel.add(button);
            button.addActionListener(buttonListener);
        }
        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        SportGuiManager.putMainFrameToCenter();
        mainFrame.setVisible(true);
    }

    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            final JButton button = (JButton) e.getSource();
            switch(button.getText()) {
                case MY_SECTIONS_BUTTON:
                    SportGuiManager.closeWindow();
                    mainFrame.add(new CoachSectionsView(mainFrame, DatabaseOperations.getCoachSections()));
                    mainFrame.validate();
                    break;
                case LAST_COMPETITIONS_BUTTON:
                    SportGuiManager.closeWindow();
                    SportGuiManager.setFutureView(SportGuiManager.getLastCompetitionView());
                    mainFrame.add(new ChooseSportTypeView(mainFrame));
                    mainFrame.validate();
                    break;
                case FUTURE_COMPETITIONS_BUTTON:
                    SportGuiManager.closeWindow();
                    SportGuiManager.setFutureView(SportGuiManager.getFutureCompetitionView());
                    mainFrame.add(new ChooseSportTypeView(mainFrame));
                    mainFrame.validate();
                    break;
                case CREATE_SECTION_BUTTON:
                    SportGuiManager.closeWindow();
                    mainFrame.add(new DayOptionsView(mainFrame));
                    validate();
                    break;
                case CREATE_CLUB_BUTTON:
                    System.out.println(CREATE_CLUB_BUTTON);
            }
        }
    }
}
