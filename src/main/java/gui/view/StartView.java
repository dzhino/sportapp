package gui.view;

import database.Roles;
import gui.manager.SportGuiManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StartView extends JPanel {
    private MainFrame mainFrame;

    private static final int FRAME_HEIGHT = 300;
    private static final int FRAME_WIDTH = 300;

    public StartView(final MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        this.mainFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        JButton loginButton = new JButton("Sign in");
        JButton regButton = new JButton("Sign Up");
        add(loginButton);
        add(regButton);
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SportGuiManager.closeWindow();
                mainFrame.add(new LoginView(mainFrame));
                mainFrame.validate();
            }
        });
        regButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SportGuiManager.closeWindow();
                mainFrame.add(new RegistrationView(mainFrame));
                mainFrame.validate();
            }
        });
        SportGuiManager.putMainFrameToCenter();
    }

}
