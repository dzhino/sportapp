package gui.view;

import database.DatabaseOperations;
import database.Roles;
import gui.manager.SportGuiManager;
import gui.manager.UserData;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class DayOptionsView extends JPanel {
    private MainFrame mainFrame;
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 300;

    private static ArrayList<String> chosenDays = new ArrayList<>();
    private static String chosenSportType;
    private static ArrayList<String> chosenSportStructure = new ArrayList<>();
    ArrayList<ArrayList<String>> places;

    private JComboBox sportTypeBox;
    private JComboBox sportStructuresBox;

    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    private static final String OK_BUTTON = "OK";
    private static final String CANCEL_BUTTON = "Cancel";
    private static final String SUNDAY = "Sunday";
    private static final String MONDAY = "Monday";
    private static final String TUESDAY = "Tuesday";
    private static final String WEDNESDAY = "Wednesday";
    private static final String THURSDAY = "Thursday";
    private static final String FRIDAY = "Friday";
    private static final String SATURDAY = "Saturday";
    private JCheckBox[] checkBoxes = {new JCheckBox(SUNDAY),
            new JCheckBox(MONDAY),
            new JCheckBox(TUESDAY),
            new JCheckBox(WEDNESDAY),
            new JCheckBox(THURSDAY),
            new JCheckBox(FRIDAY),
            new JCheckBox(SATURDAY)};

    public static ArrayList<String> getChosenDays () {
        return chosenDays;
    }

    public static String getChosenSportType() {
        return chosenSportType;
    }

    static ArrayList<String> getChosenSportStructure(){return chosenSportStructure;}

    DayOptionsView(MainFrame mainFrame){
        this.mainFrame = mainFrame;
        this.mainFrame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        JPanel mainPanel = new JPanel(new GridLayout(10, 1));
        sportTypeBox = new JComboBox();
        ArrayList<String> sportTypes = DatabaseOperations.getAvailableSportTypes();
        for (String sportType:sportTypes){
            sportTypeBox.addItem(sportType);
        }
        JPanel checkBoxesPanel = new JPanel(new GridLayout(1, 2));;
        for (int i = 0; i < checkBoxes.length; i++){
            if (i % 2 == 0)
            {
                checkBoxesPanel = new JPanel(new GridLayout(1, 2));
                checkBoxesPanel.add(checkBoxes[i]);
            } else {
                checkBoxesPanel.add(checkBoxes[i]);
                mainPanel.add(checkBoxesPanel);
            }
        }
        mainPanel.add(checkBoxesPanel);
        sportStructuresBox = new JComboBox();
        places = DatabaseOperations.getAvailablePlaces();
        for (ArrayList<String> place : places){
            sportStructuresBox.addItem(place.get(0) +" " + place.get(1));
        }
        JPanel sportTypePanel = new JPanel(new GridLayout(1, 2));
        sportTypePanel.add(new JLabel("Choose sport type"));
        sportTypePanel.add(sportTypeBox);
        mainPanel.add(sportTypePanel);

        JPanel sportStructuresPanel = new JPanel(new GridLayout(1, 2));
        sportStructuresPanel.add(new JLabel("Choose place for section"));
        sportStructuresPanel.add(sportStructuresBox);
        mainPanel.add(sportStructuresPanel);

        ButtonListener buttonListener = new ButtonListener();
        JPanel buttonPanel = new JPanel(new GridLayout(1, 2));
        JButton okButton = new JButton(OK_BUTTON);
        okButton.addActionListener(buttonListener);
        JButton cancelButton = new JButton(CANCEL_BUTTON);
        buttonPanel.add(cancelButton);
        buttonPanel.add(okButton);
        mainPanel.add(buttonPanel);
        cancelButton.addActionListener(buttonListener);
        SportGuiManager.putMainFrameToCenter();

        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        SportGuiManager.putMainFrameToCenter();
    }
    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton)e.getSource();
            if (button.getText().equals(OK_BUTTON)){
                for (JCheckBox checkBox:checkBoxes){
                    if(checkBox.isSelected()) {
                        chosenDays.add(checkBox.getText());
                    }
                }
                try {
                    if (chosenDays.size() > 0) {
                        chosenSportType = sportTypeBox.getSelectedItem().toString();
                        chosenSportStructure.add(places.get(sportStructuresBox.getSelectedIndex()).get(0));
                        chosenSportStructure.add(places.get(sportStructuresBox.getSelectedIndex()).get(1));
                        SportGuiManager.closeWindow();
                        mainFrame.add(new TimeOptionsView(mainFrame));
                        mainFrame.validate();
                    } else {
                        throw new IllegalStateException();
                    }
                }catch (IllegalStateException e1) {
                    JOptionPane.showMessageDialog(DayOptionsView.this, "You must choose days of training");
                }
            } else {
                if (UserData.getRoleId() == Roles.valueOf("COACH").mineOrdinal()) {
                    SportGuiManager.closeWindow();
                    mainFrame.add (new CoachView(mainFrame));
                    validate();
                }
            }
        }
    }
}
