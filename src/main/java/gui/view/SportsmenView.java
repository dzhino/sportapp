package gui.view;

import database.DatabaseOperations;
import gui.manager.SportGuiManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SportsmenView extends JPanel {
    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    private static final String MY_SECTIONS_BUTTON = "My sections";
    private static final String LAST_COMPETITIONS_BUTTON = "Get last competitions list";
    private static final String FUTURE_COMPETITIONS_BUTTON = "Get future competitions list";
    private static final String SEARCH_SECTION_BUTTON = "Find section";
    private static final String SEARCH_CLUB_BUTTON = "Find sport club";
    private MainFrame mainFrame;

    public SportsmenView (MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        JButton[] buttons = {new JButton(MY_SECTIONS_BUTTON),
                new JButton(LAST_COMPETITIONS_BUTTON),
                new JButton(FUTURE_COMPETITIONS_BUTTON),
                new JButton(SEARCH_SECTION_BUTTON),
                new JButton(SEARCH_CLUB_BUTTON)};

        ButtonListener buttonListener = new ButtonListener();
        JPanel mainPanel = new JPanel(new GridLayout(buttons.length, 1));
        for (JButton button : buttons) {
            mainPanel.add(button);
            button.addActionListener(buttonListener);
        }
        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        SportGuiManager.putMainFrameToCenter();
        mainFrame.setVisible(true);
    }

    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton)e.getSource();
            switch(button.getText()) {
                case MY_SECTIONS_BUTTON:
                    SportGuiManager.closeWindow();
                    mainFrame.add(new SportsmenSectionsView(mainFrame, DatabaseOperations.getSportsmenSections()));
                    mainFrame.validate();
                    break;
                case LAST_COMPETITIONS_BUTTON:
                    SportGuiManager.closeWindow();
                    SportGuiManager.setFutureView(SportGuiManager.getLastCompetitionView());
                    mainFrame.add(new ChooseSportTypeView(mainFrame));
                    mainFrame.validate();
                    break;
                case FUTURE_COMPETITIONS_BUTTON:
                    SportGuiManager.closeWindow();
                    SportGuiManager.setFutureView(SportGuiManager.getFutureCompetitionView());
                    mainFrame.add(new ChooseSportTypeView(mainFrame));
                    mainFrame.validate();
                    break;
                case SEARCH_CLUB_BUTTON:
                    System.out.println(SEARCH_CLUB_BUTTON);
                    break;
                case SEARCH_SECTION_BUTTON:
                    SportGuiManager.closeWindow();
                    SportGuiManager.setFutureView(SportGuiManager.getSectionsView());
                    mainFrame.add(new ChooseSportTypeView(mainFrame));
                    mainFrame.validate();
            }
        }
    }
}
