package gui.view;

import gui.manager.SportGuiManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class CoachInfoView extends JPanel {
    private MainFrame mainFrame;
    private ArrayList<ArrayList<String>> sections;
    private ArrayList<String> section;

    private static final String CANCEL_BUTTON = "Cancel";

    private final int OPTIONS_COUNT;
    private static final int TOP_BORDER = 5;
    private static final int BOTTOM_BORDER = 5;
    private static final int LEFT_BORDER = 5;
    private static final int RIGHT_BORDER = 5;

    CoachInfoView(MainFrame mainFrame, ArrayList<ArrayList<String>> sections, int index){
        this.mainFrame = mainFrame;
        this.sections = sections;
        section = sections.get(index);
        OPTIONS_COUNT = section.size();
        initMainPanel();
        SportGuiManager.putMainFrameToCenter();
        mainFrame.setVisible(true);
    }
    private void initMainPanel() {
        final JPanel mainPanel = new JPanel(new GridLayout (1, 1));


        ButtonListener buttonListener = new ButtonListener();


            JPanel userDataPanel = new JPanel(new GridLayout(1, section.size()));

            for(int j = 1; j < 6; j++)
            {
                JTextField stringField = new JTextField(section.get(j), section.get(j).length());
                stringField.setEnabled(false);
                userDataPanel.add(stringField);
            }

        JButton cancelButton = new JButton(CANCEL_BUTTON);
        userDataPanel.add(cancelButton);

        mainPanel.add(userDataPanel);
        cancelButton.addActionListener(buttonListener);

        JPanel paddingPanel = new JPanel();
        paddingPanel.setBorder(new EmptyBorder(TOP_BORDER, LEFT_BORDER, BOTTOM_BORDER, RIGHT_BORDER));
        paddingPanel.add(mainPanel);

        mainFrame.getContentPane().add(paddingPanel, BorderLayout.CENTER);
        mainFrame.pack();
        Dimension frameSize = mainFrame.getSize();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        mainFrame.setBounds((screenSize.width - frameSize.width)/2, (screenSize.height - frameSize.height)/2, frameSize.width, frameSize.height);
        mainFrame.setResizable(false);
    }

    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton button = (JButton)e.getSource();
            if (button.getText().equals(CANCEL_BUTTON)) {
                if(SportGuiManager.getLastView().equals(SportGuiManager.getSectionsView())) {
                    SportGuiManager.closeWindow();
                    mainFrame.add(new SectionsView(mainFrame, sections));
                    mainFrame.validate();
                } else if (SportGuiManager.getLastView().equals(SportGuiManager.getSportsmenSectionsView())){
                    SportGuiManager.closeWindow();
                    mainFrame.add(new SportsmenSectionsView(mainFrame,sections));
                    mainFrame.validate();
                } else {
                    SportGuiManager.closeWindow();
                    mainFrame.add(new CoachSectionsView(mainFrame,sections));
                    mainFrame.validate();
                }
            }
        }
    }
}
