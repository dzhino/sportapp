package database;

import gui.manager.UserData;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class DatabaseOperations {
    static private Connection CONNECTION;

    private static final String USER_ID_COLUMN = "USER_ID";
    private static final String FIRST_NAME_COLUMN = "FIRST_NAME";
    private static final String SECOND_NAME_COLUMN = "SECOND_NAME";
    private static final String THIRD_NAME_COLUMN = "THIRD_NAME";
    private static final String EMAIL_COLUMN = "EMAIL";
    private static final String PHONE_NUMBER_COLUMN = "PHONE_NUMBER";
    private static final String ROLE_ID_COLUMN = "ROLE_ID";
    private static final String SPORT_ID_COLUMN = "SPORT_ID";
    private static final String SPORT_SECTION_ID_COLUMN = "SPORT_SECTION_ID";


    DatabaseOperations(Connection connection) {
        CONNECTION = connection;
    }

    public static ArrayList<String> getAvailableStructuresTypes(){
        try{
            ArrayList<String> types = new ArrayList<>();
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT TYPE_NAME FROM STRUCTURE_TYPES");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                types.add(resultSet.getString("TYPE_NAME"));
            }
            return types;
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    private static int getStructureTypeID(String structureType){
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT TYPE_ID FROM STRUCTURE_TYPES WHERE TYPE_NAME = ?");
            statement.setString(1, structureType);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                return resultSet.getInt("TYPE_ID");
            }
        }catch (SQLException sql){
            sql.printStackTrace();
        }
        return -1;
    }

    public static void createSportStructure (String structureName, String address, String structureType) {
        try {
            int structureTypeID = getStructureTypeID(structureType);
            PreparedStatement statement = CONNECTION.prepareStatement("INSERT INTO SPORT_STRUCTURES (STRUCTURE_NAME, ADDRESS, STRUCTURE_TYPE_ID, DIRECTOR_ID) VALUES (?, ?, ?, ?)");
            statement.setString(1,structureName);
            statement.setString(2,address);
            statement.setInt(3, structureTypeID);
            statement.setInt(4, UserData.getUserId());
            statement.execute();

        } catch (SQLException sql){
            sql.printStackTrace();
        }
    }

    public static boolean isSportStructureCreated() {
        try{
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT STRUCTURE_ID FROM SPORT_STRUCTURES WHERE DIRECTOR_ID = ?");
            statement.setInt(1, UserData.getUserId());
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                return true;
            }
        } catch (SQLException sql){
            sql.printStackTrace();
        }
        return false;
    }

    public static void leaveSection(String sectionID){
        try{
            String tableName = "SECTION_PARTICIPANTS" + sectionID;
            PreparedStatement statement = CONNECTION.prepareStatement("DELETE FROM " + tableName + " WHERE SPORTSMEN_ID = ?");
            statement.setInt(1, UserData.getUserId());
            statement.execute();
        }catch (SQLException sql) {
            sql.printStackTrace();
        }
    }
    public static void dropTables (String sectionID) {
        String scheduleTableName = "SECTION_SCHEDULE" + sectionID;
        String participantsTableName = "SECTION_PARTICIPANTS" + sectionID;
        if (isTableCreated(scheduleTableName)) {
            try {
                PreparedStatement statement = CONNECTION.prepareStatement("DROP TABLE " + scheduleTableName);
                statement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (isTableCreated(participantsTableName)) {
            try {
                PreparedStatement statement = CONNECTION.prepareStatement("DROP TABLE " + participantsTableName);
                statement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("DELETE FROM SPORT_SECTIONS WHERE SPORT_SECTION_ID = ?");
            statement.setInt(1, Integer.parseInt(sectionID));
            statement.execute();
        } catch (SQLException sql){
            sql.printStackTrace();
        }
    }

    public static ArrayList<ArrayList<String>> getSchedule(String sportID){
        try {
            String tableName = "SPORT_SECTION" + sportID;
            ArrayList<ArrayList<String>> result = new ArrayList<>();
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT * FROM " + tableName);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                ArrayList<String> day = new ArrayList<>();
                day.add(resultSet.getString("DAY_NAME"));
                day.add(resultSet.getString("START_TIME"));
                day.add(resultSet.getString("FINISH_TIME"));
                result.add(day);
            }
            return result;
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    public static int getRoleID(String role) {
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT ROLE_ID " +
                    "FROM ROLES WHERE ROLE_NAME = ?");
            statement.setString(1, role);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(ROLE_ID_COLUMN);
            }
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return -1;
    }

    public static int getSportID(String sportType) {
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT SPORT_ID " +
                    "FROM SPORT_TYPES WHERE SPORT_NAME = ?");
            statement.setString(1, sportType);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(SPORT_ID_COLUMN);
            }
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return -1;
    }

    public static ArrayList<ArrayList<String >> getParticipants(String sectionID) {
        String tableName = "SECTION_PARTICIPANTS" + sectionID;
        ArrayList<ArrayList<String>> participants = new ArrayList<>();
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT USER_ID, SECOND_NAME, FIRST_NAME, THIRD_NAME, EMAIL, PHONE_NUMBER FROM " + tableName + " LEFT JOIN USERS ON SPORTSMEN_ID = USER_ID");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                ArrayList<String> participant = new ArrayList<>();
                participant.add(((Integer)resultSet.getInt(USER_ID_COLUMN)).toString());
                participant.add(resultSet.getString(SECOND_NAME_COLUMN));
                participant.add(resultSet.getString(FIRST_NAME_COLUMN));
                participant.add(resultSet.getString(THIRD_NAME_COLUMN));
                participant.add(resultSet.getString(EMAIL_COLUMN));
                participant.add(resultSet.getString(PHONE_NUMBER_COLUMN));
                participants.add(participant);
            }
            return participants;
        }catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    public static ArrayList<ArrayList<String>> getCoachSections() {

        try {
            ArrayList<ArrayList<String>> sections = new ArrayList<>();
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT SPORT_SECTION_ID FROM SPORT_SECTIONS WHERE COACH_ID = ?");
            statement.setInt(1, UserData.getUserId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int sectionID = resultSet.getInt("SPORT_SECTION_ID");
                System.out.println(sectionID);
                sections.add(getSection(sectionID));
            }
            return sections;
        }catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    private static ArrayList<Integer> getAllSectionsList() {
        try {
            ArrayList<Integer> sections = new ArrayList<>();
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT SPORT_SECTION_ID FROM SPORT_SECTIONS");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                sections.add(resultSet.getInt(SPORT_SECTION_ID_COLUMN));
            }
            return sections;
        }catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    private static ArrayList<String> getSection (int sectionID) {
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT SPORT_SECTION_ID, SECOND_NAME, FIRST_NAME, THIRD_NAME, EMAIL, PHONE_NUMBER, STRUCTURE_NAME, ADDRESS\n" +
                    "FROM (SELECT *\n" +
                    "FROM SPORT_SECTIONS INNER JOIN USERS ON SPORT_SECTIONS.COACH_ID = USERS.USER_ID\n" +
                    "WHERE SPORT_SECTION_ID = ?) INNER JOIN SPORT_STRUCTURES ON STRUCTURE_ID = SPORT_STRUCTURE_ID");
            statement.setInt(1, sectionID);
            ArrayList<String> result = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add (((Integer)resultSet.getInt(SPORT_SECTION_ID_COLUMN)).toString());
                result.add (resultSet.getString(SECOND_NAME_COLUMN));
                result.add (resultSet.getString(FIRST_NAME_COLUMN));
                result.add(resultSet.getString(THIRD_NAME_COLUMN));
                result.add(resultSet.getString(EMAIL_COLUMN));
                result.add(resultSet.getString(PHONE_NUMBER_COLUMN));
                result.add(resultSet.getString("STRUCTURE_NAME"));
                result.add(resultSet.getString("ADDRESS"));
            }
            System.out.println(result.size());
            return result;
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    public static ArrayList<ArrayList<String>> getSportsmenSections() {
        ArrayList<ArrayList<String>> sportsmenSections = new ArrayList<>();
        ArrayList<Integer> sections = getAllSectionsList();
        for (Integer sectionID : sections) {
            String tableName = "SECTION_PARTICIPANTS" + sectionID.toString();
            if (isTableCreated(tableName) && isParticipant(tableName)) {
                sportsmenSections.add(getSection(sectionID));
            }
        }
        return sportsmenSections;
    }

    public static void joinSection (String sportSectionID) {
        String tableName = "SECTION_PARTICIPANTS" + sportSectionID;
        if (!isTableCreated(tableName)){
            createParticipantsTable(tableName);
        }
        if (!isParticipant(tableName)) {
            addParticipant(tableName);
        }
        if (!isSportsmenCreated()){
            createSportsmen(Integer.parseInt(sportSectionID));
            }
        }

    private static void createSportsmen(int sectionID) {
        int sportID = getSportIdBySectionID(sectionID);
        try{
            PreparedStatement statement = CONNECTION.prepareStatement("INSERT INTO SPORTSMENS (USER_ID, SPORT_ID) VALUES (?,?)");
            statement.setInt(1, UserData.getUserId());
            statement.setInt(2, sportID);
            statement.execute();
        } catch (SQLException sql){
            sql.printStackTrace();
        }
    }

    private static int getSportIdBySectionID(int sectionID) {
        try{
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT SPORT_ID FROM SPORT_SECTIONS WHERE SPORT_SECTION_ID = ?");
            statement.setInt(1, sectionID);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("SPORT_ID");
            }
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return -1;
    }

    private static boolean isSportsmenCreated() {
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT * FROM SPORTSMENS WHERE USER_ID = ?");
            statement.setInt(1, UserData.getUserId());
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();
        } catch (SQLException sql){
            sql.printStackTrace();
            return false;
    }
}

    private static boolean isParticipant(String tableName) {
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT * FROM " + tableName + " WHERE SPORTSMEN_ID = ?");
            statement.setInt(1, UserData.getUserId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                return true;
            }
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return false;
    }

    private static void addParticipant(String tableName) {
        try{
            PreparedStatement statement = CONNECTION.prepareStatement("INSERT INTO " + tableName + " (SPORTSMEN_ID) VALUES (?)");
            statement.setInt(1, UserData.getUserId());
            statement.execute();
        }catch (SQLException sql) {
            sql.printStackTrace();
        }
    }

    private static void createParticipantsTable(String tableName) {
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("CREATE TABLE " + tableName +
                                        " (SPORTSMEN_ID    NUMBER)");
            statement.execute();
        } catch (SQLException sql){
            sql.printStackTrace();
        }
    }

    public static ArrayList<ArrayList<String>> getSectionsList (String sportType) {
        int sportID = getSportID(sportType);
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("(SELECT SPORT_SECTION_ID, SECOND_NAME, FIRST_NAME, THIRD_NAME, EMAIL,PHONE_NUMBER FROM SPORT_SECTIONS INNER JOIN USERS ON SPORT_SECTIONS.COACH_ID = USERS.USER_ID WHERE SPORT_ID = ?)");
            statement.setInt(1, sportID);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<ArrayList<String>> results = new ArrayList<>();
            while (resultSet.next()) {
                ArrayList<String> result = new ArrayList<>();
                result.add(((Integer)resultSet.getInt(SPORT_SECTION_ID_COLUMN)).toString());
                result.add(resultSet.getString(SECOND_NAME_COLUMN));
                result.add(resultSet.getString(FIRST_NAME_COLUMN));
                result.add(resultSet.getString(THIRD_NAME_COLUMN));
                result.add(resultSet.getString(EMAIL_COLUMN));
                result.add(resultSet.getString(PHONE_NUMBER_COLUMN));
                results.add(result);
            }
            return results;
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    private static int getSportSectionID(int userID, int sportID) {
        try{
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT SPORT_SECTION_ID FROM SPORT_SECTIONS WHERE (COACH_ID = ? AND SPORT_ID = ?)");
            statement.setInt(1, userID);
            statement.setInt(2, sportID);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<Integer> results = new ArrayList<>();
            while (resultSet.next()) {
                results.add (resultSet.getInt(SPORT_SECTION_ID_COLUMN));
            }
            int max = 0;
            for (int i = 0; i < results.size(); i++){
                if (max < results.get(i)) {
                    max = results.get(i);
                }
            }
            return max;
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return -1;
    }

    private static boolean isTableCreated(String tableName) {
        try{
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT TABLE_NAME FROM USER_TABLES WHERE TABLE_NAME = ?");
            statement.setString(1, tableName);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next())
            {
               return true;
            }
            else {
                return false;
            }
        }catch (SQLException sql) {
            sql.printStackTrace();
        }
        return false;
    }

    private static boolean isCoachCreated(){
        try{
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT * FROM COACHES WHERE USER_ID = ?");
            statement.setInt(1, UserData.getUserId());
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();
        }catch (SQLException sql) {
            sql.printStackTrace();
        }
        return false;
    }

    private static void createCoach(int sportID){
        try{
            System.out.println("CreateCoach");
            PreparedStatement statement = CONNECTION.prepareStatement("INSERT INTO COACHES (USER_ID, SPORT_TYPE, CLUB) VALUES (?, ?, ?)");
            statement.setInt(1, UserData.getUserId());
            statement.setInt(2, sportID);
            statement.setNull(3, Types.NULL);
            statement.execute();
        } catch (SQLException sql){
            sql.printStackTrace();
        }
    }

    static public void createSection (ArrayList<String> chosenDays, ArrayList<String> startTime, ArrayList<String> finishTime, String sportType, ArrayList<String> sportStructure) {
        try {
            int sportID = getSportID(sportType);
            int sportStructureID = getPlaceID(sportStructure);
            PreparedStatement statement = CONNECTION.prepareStatement("INSERT INTO SPORT_SECTIONS (COACH_ID, SPORT_ID, SPORT_STRUCTURE_ID) VALUES (?,?,?)");
            statement.setInt(1, UserData.getUserId());
            statement.setInt(2, sportID);
            statement.setInt(3, sportStructureID);
            statement.execute();
            int sportSectionID = getSportSectionID(UserData.getUserId(), sportID);
            String tableName = "SPORT_SECTION" + ((Integer)sportSectionID).toString();
            System.out.println(tableName);
            statement = CONNECTION.prepareStatement("CREATE TABLE " + tableName +
                    " (DAY_NAME    VARCHAR2(255), " +
                    "START_TIME  VARCHAR2(255) NOT NULL, " +
                    "FINISH_TIME VARCHAR2(255)NOT NULL)");
            statement.execute();
            for(int i = 0; i < chosenDays.size(); i++) {
                statement = CONNECTION.prepareStatement("INSERT INTO " + tableName + " (DAY_NAME, START_TIME, FINISH_TIME) VALUES (?, ?, ?)");
                statement.setString(1,chosenDays.get(i));
                statement.setString(2, startTime.get(i));
                statement.setString(3, finishTime.get(i));
                statement.execute();
            }
            tableName = "SECTION_PARTICIPANTS" + sportSectionID;
            try {
                statement = CONNECTION.prepareStatement("CREATE TABLE " + tableName + " (SPORTSMEN_ID NUMBER NOT NULL)");
                statement.execute();
            }catch (SQLException sql) {
                sql.printStackTrace();
            }
            if (!isCoachCreated()){
                createCoach(sportID);
            }
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
    }

    static public ArrayList<String> getAvailableSportTypes() {
        try {
            ArrayList<String> sportTypes = new ArrayList<>();
            Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT SPORT_NAME " +
                    "FROM SPORT_TYPES ");
            while (resultSet.next()) {
                sportTypes.add(resultSet.getString("SPORT_NAME"));
            }
            resultSet.close();
            return sportTypes;
        } catch (SQLException sql) {
            sql.printStackTrace();
            return null;
        }
    }

    static public ArrayList<String> getAvailableRoles() {
        try {
            ArrayList<String> roles = new ArrayList<>();
            Statement statement = CONNECTION.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT ROLE_NAME " +
                    "FROM ROLES ");
            while (resultSet.next()) {
                roles.add(resultSet.getString("ROLE_NAME"));
            }
            resultSet.close();
            return roles;
        } catch (SQLException sql) {
            sql.printStackTrace();
            return null;
        }
    }

    static public void Registration(String firstName, String secondName, String thirdName, String eMail, String phoneNumber, int role) {
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("INSERT INTO USERS (FIRST_NAME, SECOND_NAME, THIRD_NAME, EMAIL, PHONE_NUMBER, ROLE_ID)" +
                    " VALUES (?,?,?,?,?,?)");
            statement.setString(1, firstName);
            statement.setString(2, secondName);
            statement.setString(3, thirdName);
            statement.setString(4, eMail);
            statement.setString(5, phoneNumber);
            statement.setInt(6, role);
            statement.execute();
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
    }

    static public boolean isRegistered(String eMail) {
        try {
            PreparedStatement p_stmt = CONNECTION.prepareStatement(
                    "SELECT COUNT(*) AS ROWCOUNT FROM USERS WHERE  EMAIL = ?");
            p_stmt.setString(1, eMail);
            ResultSet resultSet = p_stmt.executeQuery();
            resultSet.next();
            int count = resultSet.getInt("ROWCOUNT");
            resultSet.close();
            return count == 1;
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return false;
    }

    static public void getUserData (String eMail) {
        try {
            PreparedStatement p_stmt = CONNECTION.prepareStatement(
                    "SELECT * FROM USERS WHERE EMAIL = ?");
            p_stmt.setString(1, eMail);
            ResultSet resultSet = p_stmt.executeQuery();
            UserData userData;
            while (resultSet.next()) {
                userData = new UserData(resultSet.getInt(USER_ID_COLUMN),
                        resultSet.getString(FIRST_NAME_COLUMN),
                        resultSet.getString(SECOND_NAME_COLUMN),
                        resultSet.getString(THIRD_NAME_COLUMN),
                        resultSet.getString(EMAIL_COLUMN),
                        resultSet.getString(PHONE_NUMBER_COLUMN),
                        resultSet.getInt(ROLE_ID_COLUMN));

            }
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
    }

    private static int getPlaceID(ArrayList<String> place){
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT STRUCTURE_ID FROM SPORT_STRUCTURES WHERE ADDRESS = ? AND STRUCTURE_NAME = ?");
            statement.setString(1, place.get(0));
            statement.setString(2, place.get(1));
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("STRUCTURE_ID");
            }
        }catch (SQLException sql) {
            sql.printStackTrace();
        }
        return -1;
    }

    public static void createCompetition(String competitionName, ArrayList <String> place, int startDay, int startMonth, int startYear, int startHour, int startMinute, int finishDay, int finishMonth, int finishYear, int finishHour, int finishMinute, String sportType) {
        try {
            int sportTypeID = getSportID(sportType);
            PreparedStatement statement = CONNECTION.prepareStatement("INSERT INTO COMPETITIONS (COMPETITION_NAME, PLACE_ID, START_DATE, END_DATE, ORGANIZER_ID, SPORT_TYPE) VALUES (?,?,?,?,?,?)");
            statement.setString(1, competitionName);
            statement.setInt(2, getPlaceID(place));
            LocalDateTime startTime = LocalDateTime.of(startYear, startMonth, startDay, startHour,startMinute);
            LocalDateTime finishTime = LocalDateTime.of(finishYear, finishMonth, finishDay, finishHour,finishMinute);
            statement.setTimestamp(3, Timestamp.valueOf(startTime));
            statement.setTimestamp(4, Timestamp.valueOf(finishTime));
            statement.setInt(5, UserData.getUserId());
            statement.setInt(6, sportTypeID);
            statement.execute();
            createCompetitionParticipantsTable(competitionName, place, startDay, startMonth, startYear, startHour, startMinute, finishDay, finishMonth, finishYear, finishHour, finishMinute, sportTypeID);
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
    }

    private static int getCompetitionID(String competitionName, ArrayList <String> place, int startDay, int startMonth, int startYear, int startHour, int startMinute, int finishDay, int finishMonth, int finishYear, int finishHour, int finishMinute, int sportTypeID) {
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT COMPETITION_ID FROM COMPETITIONS WHERE COMPETITION_NAME = ? AND PLACE_ID = ? AND START_DATE = ? AND END_DATE = ? AND ORGANIZER_ID = ? AND SPORT_TYPE = ?");
            statement.setString(1, competitionName);
            statement.setInt(2, getPlaceID(place));
            LocalDateTime startTime = LocalDateTime.of(startYear, startMonth, startDay, startHour,startMinute);
            LocalDateTime finishTime = LocalDateTime.of(finishYear, finishMonth, finishDay, finishHour,finishMinute);
            statement.setTimestamp(3, Timestamp.valueOf(startTime));
            statement.setTimestamp(4, Timestamp.valueOf(finishTime));
            statement.setInt(5, UserData.getUserId());
            statement.setInt(6, sportTypeID);
            ResultSet resultSet = statement. executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("COMPETITION_ID");
            }
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return -1;
    }

    private static void createCompetitionParticipantsTable(String competitionName, ArrayList <String> place, int startDay, int startMonth, int startYear, int startHour, int startMinute, int finishDay, int finishMonth, int finishYear, int finishHour, int finishMinute, int sportTypeID) {
        int competitionID = getCompetitionID(competitionName, place, startDay, startMonth, startYear, startHour, startMinute, finishDay, finishMonth, finishYear, finishHour, finishMinute, sportTypeID);
        if (competitionID == -1) return;
        createParticipantsTable("COMPETITION_PARTICIPANTS" + ((Integer)competitionID).toString());
    }

    public static ArrayList<ArrayList<String>> getFutureCompetitions (String sportType) {
        ArrayList<ArrayList<String>> competitions = new ArrayList<>();
        int sportTypeID = getSportID(sportType);
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT COMPETITION_ID, PLACE_ID, COMPETITION_NAME, START_DATE, END_DATE, STRUCTURE_NAME, ADDRESS\n" +
                    "FROM  COMPETITIONS INNER JOIN SPORT_STRUCTURES ON COMPETITIONS.PLACE_ID = SPORT_STRUCTURES.STRUCTURE_ID WHERE START_DATE >= ? AND SPORT_TYPE = ?");
            LocalDateTime now = LocalDateTime.now();
            statement.setTimestamp(1, Timestamp.valueOf(now));
            statement.setInt(2, sportTypeID);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ArrayList<String> competition = new ArrayList<>();
                competition.add(((Integer)resultSet.getInt("COMPETITION_ID")).toString());
                competition.add(resultSet.getString("COMPETITION_NAME"));
                competition.add(resultSet.getTimestamp("START_DATE").toString());
                competition.add(resultSet.getTimestamp("END_DATE").toString());
                competition.add(resultSet.getString("STRUCTURE_NAME"));
                competition.add(resultSet.getString("ADDRESS"));
                competitions.add(competition);
            }
        }catch (SQLException sql) {
            sql.printStackTrace();
        }
        return competitions;
    }

    public static ArrayList<ArrayList<String>> getLastCompetitions(String sportType) {
        int sportTypeID = getSportID(sportType);
        ArrayList<ArrayList<String>> competitions = new ArrayList<>();
        try{
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT COMPETITION_ID, PLACE_ID, COMPETITION_NAME, START_DATE, END_DATE, STRUCTURE_NAME, ADDRESS\n" +
                    "FROM  COMPETITIONS INNER JOIN SPORT_STRUCTURES ON COMPETITIONS.PLACE_ID = SPORT_STRUCTURES.STRUCTURE_ID WHERE END_DATE <= ? AND SPORT_TYPE = ?");
            LocalDateTime now = LocalDateTime.now();
            statement.setTimestamp(1, Timestamp.valueOf(now));
            statement.setInt(2, sportTypeID);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ArrayList<String> competition = new ArrayList<>();
                competition.add(((Integer)resultSet.getInt("COMPETITION_ID")).toString());
                competition.add(resultSet.getString("COMPETITION_NAME"));
                competition.add(resultSet.getTimestamp("START_DATE").toString());
                competition.add(resultSet.getTimestamp("END_DATE").toString());
                competition.add(resultSet.getString("STRUCTURE_NAME"));
                competition.add(resultSet.getString("ADDRESS"));
                competitions.add(competition);
            }
        }catch (SQLException sql) {
            sql.printStackTrace();
        }
        return competitions;
    }

    public static ArrayList<ArrayList<String>> getAvailablePlaces() {
        try {
            ArrayList <ArrayList <String>> places = new ArrayList<>();
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT ADDRESS, STRUCTURE_NAME FROM SPORT_STRUCTURES");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ArrayList <String> place = new ArrayList<>();
                place.add(resultSet.getString("ADDRESS"));
                place.add(resultSet.getString("STRUCTURE_NAME"));
                places.add(place);
            }
            return places;
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    public static void joinCompetition(String competitionID) {
        String tableName = "COMPETITION_PARTICIPANTS" + competitionID;
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("INSERT INTO " + tableName + " (SPORTSMEN_ID) VALUES (?)");
            statement.setInt(1, UserData.getUserId());
            statement.execute();
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
    }

    public static ArrayList<ArrayList<String>> getFutureOrganizerCompetitions(String sportType) {
        int sportTypeID = getSportID(sportType);
        ArrayList<ArrayList<String>> competitions = new ArrayList<>();
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT COMPETITION_ID, PLACE_ID, COMPETITION_NAME, START_DATE, END_DATE, STRUCTURE_NAME, ADDRESS\n" +
                    "FROM  COMPETITIONS INNER JOIN SPORT_STRUCTURES ON COMPETITIONS.PLACE_ID = SPORT_STRUCTURES.STRUCTURE_ID WHERE START_DATE >= ? AND SPORT_TYPE = ? AND ORGANIZER_ID = ?");
            LocalDateTime now = LocalDateTime.now();
            statement.setTimestamp(1, Timestamp.valueOf(now));
            statement.setInt(2, sportTypeID);
            statement.setInt(3, UserData.getUserId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                ArrayList<String> competition = new ArrayList<>();
                competition.add(((Integer)resultSet.getInt("COMPETITION_ID")).toString());
                competition.add(resultSet.getString("COMPETITION_NAME"));
                competition.add(resultSet.getTimestamp("START_DATE").toString());
                competition.add(resultSet.getTimestamp("END_DATE").toString());
                competition.add(resultSet.getString("STRUCTURE_NAME"));
                competition.add(resultSet.getString("ADDRESS"));
                competitions.add(competition);
            }
            return competitions;
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    public static ArrayList<ArrayList<String>> getCompetitionParticipants(String competition) {
        String tableName = "COMPETITION_PARTICIPANTS" + competition;
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT USER_ID, SECOND_NAME, FIRST_NAME, THIRD_NAME, EMAIL, PHONE_NUMBER\n" +
                    "FROM " + tableName + " INNER JOIN USERS ON SPORTSMEN_ID = USER_ID");
            ResultSet resultSet = statement.executeQuery();
            ArrayList<ArrayList<String>> participants = new ArrayList<>();
            while (resultSet.next()) {
                ArrayList<String> participant = new ArrayList<>();
                participant.add(((Integer)resultSet.getInt(USER_ID_COLUMN)).toString());
                participant.add(resultSet.getString(SECOND_NAME_COLUMN));
                participant.add(resultSet.getString(FIRST_NAME_COLUMN));
                participant.add(resultSet.getString(THIRD_NAME_COLUMN));
                participant.add(resultSet.getString(EMAIL_COLUMN));
                participant.add(resultSet.getString(PHONE_NUMBER_COLUMN));
                participants.add(participant);
            }
            return participants;
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    public static ArrayList<ArrayList<String>> getLastOrganizerCompetitions(String sportType) {
        int sportTypeID = getSportID(sportType);
        ArrayList<ArrayList<String>> competitions = new ArrayList<>();
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT COMPETITION_ID, PLACE_ID, COMPETITION_NAME, START_DATE, END_DATE, STRUCTURE_NAME, ADDRESS\n" +
                    "FROM  COMPETITIONS INNER JOIN SPORT_STRUCTURES ON COMPETITIONS.PLACE_ID = SPORT_STRUCTURES.STRUCTURE_ID WHERE START_DATE <= ? AND SPORT_TYPE = ? AND ORGANIZER_ID = ?");
            LocalDateTime now = LocalDateTime.now();
            statement.setTimestamp(1, Timestamp.valueOf(now));
            statement.setInt(2, sportTypeID);
            statement.setInt(3, UserData.getUserId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                ArrayList<String> competition = new ArrayList<>();
                competition.add(((Integer)resultSet.getInt("COMPETITION_ID")).toString());
                competition.add(resultSet.getString("COMPETITION_NAME"));
                competition.add(resultSet.getTimestamp("START_DATE").toString());
                competition.add(resultSet.getTimestamp("END_DATE").toString());
                competition.add(resultSet.getString("STRUCTURE_NAME"));
                competition.add(resultSet.getString("ADDRESS"));
                competitions.add(competition);
            }
            return competitions;
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    public static ArrayList<ArrayList<String>> getFutureCompetitionsByDirectorID() {
        int placeID = getPlaceIdByDirectorID();
        ArrayList<ArrayList<String>> competitions = new ArrayList<>();
        try{
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT COMPETITION_NAME, START_DATE, END_DATE FROM COMPETITIONS WHERE PLACE_ID = ? AND END_DATE >= ? ORDER BY START_DATE");
            statement.setInt(1, placeID);
            LocalDateTime now = LocalDateTime.now();
            statement.setTimestamp(2, Timestamp.valueOf(now));
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()) {
                ArrayList<String> competition = new ArrayList<>();
                competition.add(resultSet.getString("COMPETITION_NAME"));
                competition.add(resultSet.getString("START_DATE"));
                competition.add(resultSet.getString("END_DATE"));
                competitions.add(competition);
            }
            return competitions;
        }catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }

    private static int getPlaceIdByDirectorID() {
        try {
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT STRUCTURE_ID FROM SPORT_STRUCTURES WHERE DIRECTOR_ID = ?");
            statement.setInt(1, UserData.getUserId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("STRUCTURE_ID");
            }
        } catch (SQLException sql){
            sql.printStackTrace();
        }
        return -1;
    }

    public static ArrayList<String> getStructuresByType(String structureType) {
        int structureTypeID = getStructureTypeID(structureType);
        try{
            PreparedStatement statement = CONNECTION.prepareStatement("SELECT ADDRESS, STRUCTURE_NAME FROM SPORT_STRUCTURES WHERE STRUCTURE_TYPE_ID = ?");
            statement.setInt(1, structureTypeID);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<String> results = new ArrayList<>();
            while (resultSet.next()){
                results.add(resultSet.getString("ADDRESS")  + " " + resultSet.getString("STRUCTURE_NAME"));
            }
            return results;
        } catch (SQLException sql) {
            sql.printStackTrace();
        }
        return null;
    }
}
