package database;
import java.sql.*;
import java.util.Locale;

public class BaseConnection {
    private static final String url = "jdbc:oracle:thin:@84.237.50.81:1521:XE";
    private static final String user = "E.PAVLOV1";
    private static final String password = "1rjwbXoa";

    /*private static final String url = "jdbc:oracle:thin:@localhost:1521:XE";
    private static final String user = "Sport";
    private static final String password = "1rjwbXoa";*/

    public void init() {
        try {
            Locale.setDefault(Locale.ENGLISH);
            Connection connection = DriverManager.getConnection(url, user, password);
            new DatabaseOperations(connection);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

}
