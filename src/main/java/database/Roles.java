package database;

public enum Roles {
    SPORTSMEN("Sportsmen", "Спортсмен"),
    COACH("Coach", "Тренер"),
    ORGANIZER("Organizer", "Организатор"),
    DIRECTOR("Director", "Директор спортивного сооружения");

    private String name;
    private String ob;

    Roles(String name, String ob) {
        this.name = name;
        this.ob = ob;
    }
    public int mineOrdinal () {
        return this.ordinal() + 1;
    }
    public String getOb() {
        return ob;
    }
    }

