import database.BaseConnection;

import gui.manager.*;

public class SportClient {
    public static void main(String[] args) {
        new BaseConnection().init();
        SportGuiManager sgm = new SportGuiManager();
        sgm.start();
    }
}
